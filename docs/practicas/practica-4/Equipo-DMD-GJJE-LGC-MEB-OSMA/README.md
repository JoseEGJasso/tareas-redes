# Práctica 4 - Equipo-DMD-GJJE-LGC-MEB-OSMA

## Integrantes:

- Dozal Magnani Diego - 316032708 - @diegodmag
- González Jasso José Eduardo - 316093837 - @JoseEGJasso
- Lorenzo Guerrero Celeste - 316162027 - @CelesteLG
- Martinez Enriquez Bruno - 316126597 - @BrunoMartz
- Ordóñez Silis Miguel Ángel - 417034052 - @SirMaik

## Explicación de la topología
| ![](img/topologia.png)|
|:------------------------:|
| Topología |


## Comandos de Router

### Router 0

**Nombre de host:** router-X

[Archivo `startup_config`](./files/Routers/Router0_startup-config.txt)

- Comando `show cdp neighbors`:
- ```text
    Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge
                      S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone
    Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
    router-IG    Gig 2/0          146            R       PT1000      Gig 0/0
    router-IGF   Gig 3/0          146            R       PT1000      Gig 0/0
    router-IQ    Gig 0/0          146            R       PT1000      Gig 0/0
    router-AI    Gig 1/0          146            R       PT1000      Gig 0/0
  ```
- Comando `show ip arp`:
- ```text
  Protocol  Address          Age (min)  Hardware Addr   Type   Interface
  Internet  198.51.100.1            -   00D0.BA31.B15D  ARPA   GigabitEthernet0/0
  Internet  198.51.100.9            -   000B.BED8.D7BD  ARPA   GigabitEthernet1/0
  Internet  198.51.100.17           -   0001.6338.C032  ARPA   GigabitEthernet2/0
  Internet  198.51.100.25           -   0090.0CE3.5EDC  ARPA   GigabitEthernet3/0
  Internet  198.51.100.33           -   00E0.F904.5267  ARPA   GigabitEthernet4/0
```
- Comando `show ip interface brief`:
```text
    Interface              IP-Address      OK? Method Status                Protocol
    GigabitEthernet0/0     198.51.100.1    YES NVRAM  up                    up
    GigabitEthernet1/0     198.51.100.9    YES NVRAM  up                    up
    GigabitEthernet2/0     198.51.100.17   YES NVRAM  up                    up
    GigabitEthernet3/0     198.51.100.25   YES NVRAM  up                    up
    GigabitEthernet4/0     198.51.100.33   YES NVRAM  up                    up
```
- Comando `show ip route`
- ```text
    - Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
           D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
           N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
           E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
           i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
           * - candidate default, U - per-user static route, o - ODR
           P - periodic downloaded static route

    Gateway of last resort is not set

    R    10.0.0.0/8 [120/1] via 198.51.100.18, 00:00:07, GigabitEthernet2/0
                    [120/1] via 198.51.100.26, 00:00:10, GigabitEthernet3/0
    R    192.168.3.0/24 [120/1] via 198.51.100.18, 00:00:07, GigabitEthernet2/0
    R    192.168.4.0/24 [120/1] via 198.51.100.26, 00:00:10, GigabitEthernet3/0
         198.51.100.0/29 is subnetted, 5 subnets
    C       198.51.100.0 is directly connected, GigabitEthernet0/0
    C       198.51.100.8 is directly connected, GigabitEthernet1/0
    C       198.51.100.16 is directly connected, GigabitEthernet2/0
    C       198.51.100.24 is directly connected, GigabitEthernet3/0
    C       198.51.100.32 is directly connected, GigabitEthernet4/0
 ```
- Comando `show ip route summary`
```text
    IP routing table name is Default-IP-Routing-Table(0)
    IP routing table maximum-paths is 16
    Route Source    Networks    Subnets     Overhead    Memory (bytes)
    connected       0           5           360         640
    static          0           0           0           0
    rip             3           0           216         384
    internal        1                                   1148
    Total           4           5           576         2172
```


### Router 1

- Comando `show cdp neighbors`:
```text
Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge
                  S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
Switch       Fas 2/0          128            S       PT3000      Fas 0/1
Router       Gig 0/0          128            R       PT1000      Gig 0/0
Switch       Fas 1/0          128            S       PT3000      Fas 0/1
```
- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  10.0.1.254              -   0003.E402.D917  ARPA   FastEthernet2/0
Internet  192.168.1.1             30  0060.70DA.3288  ARPA   FastEthernet1/0
Internet  192.168.1.254           -   00D0.BA52.4D96  ARPA   FastEthernet1/0
Internet  198.51.100.2            -   00E0.8FB9.42A0  ARPA   GigabitEthernet0/0
```
- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.2    YES NVRAM  up                    up
FastEthernet1/0        192.168.1.254   YES NVRAM  up                    up
FastEthernet2/0        10.0.1.254      YES NVRAM  up                    up
```
- Comando `show ip route`
```text
Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/8 is variably subnetted, 2 subnets, 2 masks
R       10.0.0.0/8 [120/2] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
C       10.0.1.0/24 is directly connected, FastEthernet2/0
C    192.168.1.0/24 is directly connected, FastEthernet1/0
R    192.168.3.0/24 [120/2] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
R    192.168.4.0/24 [120/2] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
     198.51.100.0/29 is subnetted, 5 subnets
C       198.51.100.0 is directly connected, GigabitEthernet0/0
R       198.51.100.8 [120/1] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
R       198.51.100.16 [120/1] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
R       198.51.100.24 [120/1] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
R       198.51.100.32 [120/1] via 198.51.100.1, 00:00:10, GigabitEthernet0/0
```
- Comando `show ip route summary`
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          0           0           0           0
rip             2           5           504         896
internal        2                                   2296
Total           5           7           720         3576
```
- Comando `show ip dhcp pull`
```text
Pool LAN1 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0
 Total addresses                : 254
 Leased addresses               : 1
 Excluded addresses             : 1
 Pending event                  : none

 1 subnet is currently in the pool
 Current index        IP address range                    Leased/Excluded/Total
 192.168.1.1          192.168.1.1      - 192.168.1.254     1    / 1     / 254
```

- Comando `show ip nat statistics`
```text
Total translations: 1 (1 static, 0 dynamic, 0 extended)
Outside Interfaces: GigabitEthernet0/0
Inside Interfaces: FastEthernet1/0 , FastEthernet2/0
Hits: 48  Misses: 162
Expired translations: 16
Dynamic mappings:
- Comando `show ip nat translations`
Pro  Inside global     Inside local       Outside local      Outside global
---  198.51.100.3      10.0.1.1           ---                ---
```
- Comando `show ip rip database`
```text
10.0.0.0/8    auto-summary
10.0.0.0/8
    [2] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
192.168.3.0/24    auto-summary
192.168.3.0/24
    [2] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
192.168.4.0/24    auto-summary
192.168.4.0/24
    [2] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
198.51.100.0/29    auto-summary
198.51.100.0/29    directly connected, GigabitEthernet0/0
198.51.100.8/29    auto-summary
198.51.100.8/29
    [1] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
198.51.100.16/29    auto-summary
198.51.100.16/29
    [1] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
198.51.100.24/29    auto-summary
198.51.100.24/29
    [1] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
198.51.100.32/29    auto-summary
198.51.100.32/29
    [1] via 198.51.100.1, 00:00:16, GigabitEthernet0/0
```

### Router 2

**Nombre de host:** router-AI

[Archivo `startup_config`](./files/Routers/Router1_startup-config.txt)

- Comando `show cdp neighbors`:
  ```text
      Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
    Switch       Fas 1/0          146            S       PT3000      Fas 0/1
    Switch       Fas 2/0          146            S       PT3000      Fas 0/1
    Router       Gig 0/0          146            R       PT1000      Gig 1/0
  ```
- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  10.0.2.254              -   000D.BD4B.E1B4  ARPA   FastEthernet2/0
Internet  192.168.2.254           -   00E0.F712.ABC3  ARPA   FastEthernet1/0
Internet  198.51.100.9            4   000B.BED8.D7BD  ARPA   GigabitEthernet0/0
Internet  198.51.100.10           -   0001.437E.11AB  ARPA   GigabitEthernet0/0
```
- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.10   YES NVRAM  up                    up
FastEthernet1/0        192.168.2.254   YES NVRAM  up                    up
FastEthernet2/0        10.0.2.254      YES NVRAM  up                    up
```
- Comando `show ip route`
```text
Gateway of last resort is not set

     10.0.0.0/8 is variably subnetted, 2 subnets, 2 masks
R       10.0.0.0/8 [120/2] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
C       10.0.2.0/24 is directly connected, FastEthernet2/0
C    192.168.2.0/24 is directly connected, FastEthernet1/0
R    192.168.3.0/24 [120/2] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
R    192.168.4.0/24 [120/2] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
     198.51.100.0/29 is subnetted, 5 subnets
R       198.51.100.0 [120/1] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
C       198.51.100.8 is directly connected, GigabitEthernet0/0
R       198.51.100.16 [120/1] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
R       198.51.100.24 [120/1] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
R       198.51.100.32 [120/1] via 198.51.100.9, 00:00:03, GigabitEthernet0/0
```
- Comando `show ip route summary`
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          0           0           0           0
rip             2           5           504         896
internal        2                                   2296
Total           5           7           720         3576
```
- Comando `show ip dhcp pool`
```text
Pool LAN2 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0
 Total addresses                : 254
 Leased addresses               : 1
 Excluded addresses             : 0
 Pending event                  : none

 1 subnet is currently in the pool
 Current index        IP address range                    Leased/Excluded/Total
 192.168.2.1          192.168.2.1      - 192.168.2.254     1    / 0     / 254
```

- Comando `show ip nat statistics`
```text
    Total translations: 1 (1 static, 0 dynamic, 0 extended)
    Outside Interfaces: GigabitEthernet0/0
    Inside Interfaces: FastEthernet1/0 , FastEthernet2/0
    Hits: 42  Misses: 120
    Expired translations: 15
    Dynamic mappings:
```
- Comando `show ip nat translations`
```text
    Total translations: 1 (1 static, 0 dynamic, 0 extended)
    Outside Interfaces: GigabitEthernet0/0
    Inside Interfaces: FastEthernet1/0 , FastEthernet2/0
    Hits: 42  Misses: 120
    Expired translations: 15
    Dynamic mappings:
    router-AI>show ip nat translations
    Pro  Inside global     Inside local       Outside local      Outside global
    ---  198.51.100.11     10.0.2.1           ---                ---
```
- Comando `show ip rip database`
```text
10.0.0.0/8    auto-summary
10.0.0.0/8
    [2] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
192.168.3.0/24    auto-summary
192.168.3.0/24
    [2] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
192.168.4.0/24    auto-summary
192.168.4.0/24
    [2] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
198.51.100.0/29    auto-summary
198.51.100.0/29
    [1] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
198.51.100.8/29    auto-summary
198.51.100.8/29    directly connected, GigabitEthernet0/0
198.51.100.16/29    auto-summary
198.51.100.16/29
    [1] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
198.51.100.24/29    auto-summary
198.51.100.24/29
    [1] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
198.51.100.32/29    auto-summary
198.51.100.32/29
    [1] via 198.51.100.9, 00:00:26, GigabitEthernet0/0
```

### Router 3

[Archivo `startup_config`](./files/Routers/Router3_startup-config.txt)

- Comando `show cdp neighbors`:
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
Switch       Fas 2/0          155            S       PT3000      Fas 0/1
Switch       Fas 1/0          155            S       PT3000      Fas 0/1
Router       Gig 0/0          155            R       PT1000      Gig 2/0
```
- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  10.0.3.254              -   0060.5C25.335E  ARPA   FastEthernet2/0
Internet  192.168.3.254           -   000D.BD98.019D  ARPA   FastEthernet1/0
Internet  198.51.100.17           12  0001.6338.C032  ARPA   GigabitEthernet0/0
Internet  198.51.100.18           -   0001.63C6.661E  ARPA   GigabitEthernet0/0
```
- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.18   YES NVRAM  up                    up
FastEthernet1/0        192.168.3.254   YES NVRAM  up                    up
FastEthernet2/0        10.0.3.254      YES NVRAM  up                    up
```
- Comando `show ip route`
```text
Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 1 subnets
C       10.0.3.0 is directly connected, FastEthernet2/0
C    192.168.3.0/24 is directly connected, FastEthernet1/0
R    192.168.4.0/24 [120/2] via 198.51.100.17, 00:00:25, GigabitEthernet0/0
     198.51.100.0/29 is subnetted, 5 subnets
R       198.51.100.0 [120/1] via 198.51.100.17, 00:00:25, GigabitEthernet0/0
R       198.51.100.8 [120/1] via 198.51.100.17, 00:00:25, GigabitEthernet0/0
C       198.51.100.16 is directly connected, GigabitEthernet0/0
R       198.51.100.24 [120/1] via 198.51.100.17, 00:00:25, GigabitEthernet0/0
R       198.51.100.32 [120/1] via 198.51.100.17, 00:00:25, GigabitEthernet0/0
```
- Comando `show ip route summary`
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          0           0           0           0
rip             1           4           360         640
internal        2                                   2296
Total           4           6           576         3320
```
- Comando `show ip dhcp pool`
```text
Pool LAN3 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0
 Total addresses                : 254
 Leased addresses               : 1
 Excluded addresses             : 0
 Pending event                  : none

 1 subnet is currently in the pool
 Current index        IP address range                    Leased/Excluded/Total
 192.168.3.1          192.168.3.1      - 192.168.3.254     1    / 0     / 254
```

- Comando `show ip nat statistics`
```text
Total translations: 1 (1 static, 0 dynamic, 0 extended)
Outside Interfaces: GigabitEthernet0/0
Inside Interfaces: FastEthernet1/0 , FastEthernet2/0
Hits: 22  Misses: 227
Expired translations: 3
Dynamic mappings:
```
- Comando `show ip nat translations`
```text
Pro  Inside global     Inside local       Outside local      Outside global
---  198.51.100.19     10.0.3.1           ---                ---
```
- Comando `show ip rip database`
```text
10.0.3.0/24    auto-summary
10.0.3.0/24    directly connected, FastEthernet2/0
192.168.3.0/24    auto-summary
192.168.3.0/24    directly connected, FastEthernet1/0
192.168.4.0/24    auto-summary
192.168.4.0/24
    [2] via 198.51.100.17, 00:00:08, GigabitEthernet0/0
198.51.100.0/29    auto-summary
198.51.100.0/29
    [1] via 198.51.100.17, 00:00:08, GigabitEthernet0/0
198.51.100.8/29    auto-summary
198.51.100.8/29
    [1] via 198.51.100.17, 00:00:08, GigabitEthernet0/0
198.51.100.16/29    auto-summary
198.51.100.16/29    directly connected, GigabitEthernet0/0
198.51.100.24/29    auto-summary
198.51.100.24/29
    [1] via 198.51.100.17, 00:00:08, GigabitEthernet0/0
198.51.100.32/29    auto-summary
198.51.100.32/29
    [1] via 198.51.100.17, 00:00:08, GigabitEthernet0/0
```

### Router 4

[Archivo `startup_config`](./files/Routers/Router4_startup-config.txt)

- Comando `show cdp neighbors`:
```text
Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge
                  S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
Switch       Fas 2/0          138            S       PT3000      Fas 0/1
Switch       Fas 1/0          137            S       PT3000      Fas 0/1
Router       Gig 0/0          137            R       PT1000      Gig 3/0
```
- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  10.0.4.254              -   00E0.F765.08E4  ARPA   FastEthernet2/0
Internet  192.168.4.1             45  0009.7C40.7740  ARPA   FastEthernet1/0
Internet  192.168.4.254           -   00D0.BAC4.E045  ARPA   FastEthernet1/0
Internet  198.51.100.26           -   0001.420C.E233  ARPA   GigabitEthernet0/0
```
- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.26   YES manual up                    up
FastEthernet1/0        192.168.4.254   YES NVRAM  up                    up
FastEthernet2/0        10.0.4.254      YES NVRAM  up                    up
```
- Comando `show ip route`
```text
Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 1 subnets
C       10.0.4.0 is directly connected, FastEthernet2/0
R    192.168.3.0/24 [120/2] via 198.51.100.25, 00:00:03, GigabitEthernet0/0
C    192.168.4.0/24 is directly connected, FastEthernet1/0
     198.51.100.0/29 is subnetted, 5 subnets
R       198.51.100.0 [120/1] via 198.51.100.25, 00:00:03, GigabitEthernet0/0
R       198.51.100.8 [120/1] via 198.51.100.25, 00:00:03, GigabitEthernet0/0
R       198.51.100.16 [120/1] via 198.51.100.25, 00:00:03, GigabitEthernet0/0
C       198.51.100.24 is directly connected, GigabitEthernet0/0
R       198.51.100.32 [120/1] via 198.51.100.25, 00:00:03, GigabitEthernet0/0
```
- Comando `show ip route summary`
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          0           0           0           0
rip             1           4           360         640
internal        2                                   2296
Total           4           6           576         3320
```
- Comando `show ip dhcp pool`
```text
Pool LAN4 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0
 Total addresses                : 254
 Leased addresses               : 1
 Excluded addresses             : 0
 Pending event                  : none

 1 subnet is currently in the pool
 Current index        IP address range                    Leased/Excluded/Total
 192.168.4.1          192.168.4.1      - 192.168.4.254     1    / 0     / 254
```

- Comando `show ip nat statistics`
```text
Total translations: 1 (1 static, 0 dynamic, 0 extended)
Outside Interfaces: GigabitEthernet0/0
Inside Interfaces: FastEthernet1/0 , FastEthernet2/0
Hits: 23  Misses: 318
Expired translations: 4
Dynamic mappings:
```
- Comando `show ip nat translations`
```text
Pro  Inside global     Inside local       Outside local      Outside global
---  198.51.100.27     10.0.4.1           ---                ---

```
- Comando `show ip rip database`
```text
10.0.4.0/24    auto-summary
10.0.4.0/24    directly connected, FastEthernet2/0
192.168.3.0/24    auto-summary
192.168.3.0/24
    [2] via 198.51.100.25, 00:00:17, GigabitEthernet0/0
192.168.4.0/24    auto-summary
192.168.4.0/24    directly connected, FastEthernet1/0
198.51.100.0/29    auto-summary
198.51.100.0/29
    [1] via 198.51.100.25, 00:00:17, GigabitEthernet0/0
198.51.100.8/29    auto-summary
198.51.100.8/29
    [1] via 198.51.100.25, 00:00:17, GigabitEthernet0/0
198.51.100.16/29    auto-summary
198.51.100.16/29
    [1] via 198.51.100.25, 00:00:17, GigabitEthernet0/0
198.51.100.24/29    auto-summary
198.51.100.24/29    directly connected, GigabitEthernet0/0
198.51.100.32/29    auto-summary
198.51.100.32/29
    [1] via 198.51.100.25, 00:00:17, GigabitEthernet0/0
```




## Pruebas de conectividad

**Ping entre laptop 1 y Router 4**
    C:\>ping 198.51.100.26

    Pinging 198.51.100.26 with 32 bytes of data:

    Reply from 198.51.100.26: bytes=32 time=1ms TTL=253
    Reply from 198.51.100.26: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.26: bytes=32 time=13ms TTL=253
    Reply from 198.51.100.26: bytes=32 time<1ms TTL=253

    Ping statistics for 198.51.100.26:
        Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
    Approximate round trip times in milli-seconds:
        Minimum = 0ms, Maximum = 13ms, Average = 3ms

**Ping entre laptop 3 y Router 2**
    C:\>ping 198.51.100.10

    Pinging 198.51.100.10 with 32 bytes of data:

    Reply from 198.51.100.10: bytes=32 time=10ms TTL=253
    Reply from 198.51.100.10: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.10: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.10: bytes=32 time<1ms TTL=253

    Ping statistics for 198.51.100.10:
        Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
    Approximate round trip times in milli-seconds:
        Minimum = 0ms, Maximum = 10ms, Average = 2ms

**Ping entre laptop 2 y Router 3**
    C:\>ping 198.51.100.18

    Pinging 198.51.100.18 with 32 bytes of data:

    Reply from 198.51.100.18: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.18: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.18: bytes=32 time<1ms TTL=253
    Reply from 198.51.100.18: bytes=32 time<1ms TTL=253

    Ping statistics for 198.51.100.18:
        Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
    Approximate round trip times in milli-seconds:
        Minimum = 0ms, Maximum = 0ms, Average = 0ms

### Pruebas de conectividad a la IP reservada del NAT estatico

| ![](img/router1_2.png)|
|:------------------------:|
| Conexion de la laptop 1 al recurso del servidor DMZ de la LAN del Router 2 |

| ![](img/router1_3.png)|
|:------------------------:|
| Conexion de la laptop 1 al recurso del servidor DMZ de la LAN del Router 3 |

| ![](img/router1_4.png)|
|:------------------------:|
| Conexion de la laptop 1 al recurso del servidor DMZ de la LAN del Router 4 |

| ![](img/router2_3.png)|
|:------------------------:|
| Conexion de la laptop 2 al recurso del servidor DMZ de la LAN del Router 3|

| ![](img/router2_4.png)|
|:------------------------:|
| Conexion de la laptop 2 al recurso del servidor DMZ de la LAN del Router 4|

| ![](img/router3_4.png)|
|:------------------------:|
| Conexion de la laptop 3 al recurso del servidor DMZ de la LAN del Router 4|
