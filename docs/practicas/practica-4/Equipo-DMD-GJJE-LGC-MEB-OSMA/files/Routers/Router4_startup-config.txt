!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname router-IGF
!
!
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
!
!
!
ip dhcp pool LAN4
 network 192.168.4.0 255.255.255.0
 default-router 192.168.4.254
 dns-server 198.51.100.34
!
!
!
no ip cef
no ipv6 cef
!
!
!
username netadmin password 0 netadmin
!
!
!
!
!
!
!
!
ip ssh version 2
ip domain-name cisco.com
ip name-server 198.51.100.34
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 description WAN-IG
 ip address 198.51.100.26 255.255.255.248
 ip nat outside
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address 192.168.4.254 255.255.255.0
 ip nat inside
 duplex auto
 speed auto
!
interface FastEthernet2/0
 ip address 10.0.4.254 255.255.255.0
 ip nat inside
 duplex auto
 speed auto
!
router rip
 version 2
 network 10.0.0.0
 network 192.168.4.0
 network 198.51.100.0
!
ip nat inside source list 1 interface GigabitEthernet0/0 overload
ip nat inside source static 10.0.4.1 198.51.100.27
ip classless
!
ip flow-export version 9
!
!
access-list 1 permit 192.168.4.0 0.0.0.255
!
banner motd #
*********** WARNING ***********

Access to this device is restricted to authorized persons only! Violators will be PROSECUTED!

**************************************************#
!
!
!
!
!
line con 0
 password sanfran
 login
!
line aux 0
!
line vty 0 4
 password sanjose
 login local
 transport input ssh
!
!
!
end
