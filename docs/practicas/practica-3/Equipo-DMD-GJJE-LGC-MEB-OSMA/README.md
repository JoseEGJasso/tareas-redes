 # Práctica 3 - Equipo-DMD-GJJE-LGC-MEB-OSMA

## Integrantes:

- Dozal Magnani Diego - 316032708 - @diegodmag
- González Jasso José Eduardo - 316093837 - @JoseEGJasso
- Lorenzo Guerrero Celeste - 316162027 - @CelesteLG
- Martinez Enriquez Bruno - 316126597 - @BrunoMartz
- Ordóñez Silis Miguel Ángel - 417034052 - @SirMaik

---
## Explicación de la topología
Para esta práctica supusimos un escenario donde el Anexo de Ingeniería, el Instituto de Química, el Instituto de Geografía y el Instituto de Geofísica quieren establecer una red entre ellos. Para esto decidimos ocupar una topología de malla de la siguiente manera:
| ![](img/croquis2.png)|
|:------------------------:|
| Red de las dependencias |

De esta forma todos las dependencias se pueden comunicar entre sí y solo el Instituto de Química está conectado directamente con el Anexo de Ingeniería por estar ambos relativamente cerca y ya no necesitamos conectarlo con los demás Institutos que están más alejados.
| ![](img/topologia.png)|
|:------------------------:|
| Topología de la red |


## Información de equipos

### Router 1

**Nombre de host:** router-IQ

[Archivo `startup_config`](./files/Router1_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**

```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-AI    Gig 1/0          124            R       PT1000      Gig 0/0
router-IG    Gig 0/0          158            R       PT1000      Gig 0/0
Switch       Fas 3/0          158            S       PT3000      Fas 0/1
router-IGF   Gig 2/0          158            R       PT1000      Gig 2/0
```

- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.1.1             7   0060.70DA.3288  ARPA   FastEthernet3/0
Internet  192.168.1.254           -   00E0.8FC0.070B  ARPA   FastEthernet3/0
Internet  198.51.100.1            -   00E0.B096.E384  ARPA   GigabitEthernet1/0
Internet  198.51.100.2            8   0001.437E.11AB  ARPA   GigabitEthernet1/0
Internet  198.51.100.5            -   00E0.8FB9.42A0  ARPA   GigabitEthernet0/0
Internet  198.51.100.6            7   0001.63C6.661E  ARPA   GigabitEthernet0/0
Internet  198.51.100.9            -   00E0.A30E.0274  ARPA   GigabitEthernet2/0
Internet  198.51.100.10           7   00E0.8F33.3E7B  ARPA   GigabitEthernet2/0
```

- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.5    YES manual up                    up
GigabitEthernet1/0     198.51.100.1    YES manual up                    up
GigabitEthernet2/0     198.51.100.9    YES manual up                    up
FastEthernet3/0        192.168.1.254   YES manual up                    up
```

- Comando `show ip route`:
```text
Gateway of last resort is not set

     192.51.100.0/30 is subnetted, 1 subnets
S       192.51.100.12 [1/0] via 198.51.100.6
                      [1/0] via 198.51.100.10
C    192.168.1.0/24 is directly connected, FastEthernet3/0
S    192.168.2.0/24 [1/0] via 198.51.100.2
S    192.168.3.0/24 [1/0] via 198.51.100.6
S    192.168.4.0/24 [1/0] via 198.51.100.10
     198.51.100.0/30 is subnetted, 3 subnets
C       198.51.100.0 is directly connected, GigabitEthernet1/0
C       198.51.100.4 is directly connected, GigabitEthernet0/0
C       198.51.100.8 is directly connected, GigabitEthernet2/0
```
- Comando `show ip route summary`
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           3           288         512
static          3           1           288         512
rip             0           0           0           0
internal        2                                   2296
Total           6           4           576         3320
```

### Switch 1

**IP de administración:** 192.168.1.253

[Archivo `startup_config`](./files/Switch-LAN-1_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-IQ    Fas 0/1          132            R       PT1000      Fas 3/0
```

- Comando `show ip arp`:
```text
Internet  192.168.1.253           -   0001.9692.1130  ARPA   Vlan1
```

- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
FastEthernet0/1        unassigned      YES manual up                    up
FastEthernet1/1        unassigned      YES manual up                    up
FastEthernet2/1        unassigned      YES manual down                  down
FastEthernet3/1        unassigned      YES manual down                  down
FastEthernet4/1        unassigned      YES manual down                  down
FastEthernet5/1        unassigned      YES manual down                  down
Vlan1                  192.168.1.253   YES manual up                    up
```

------------------------------

### Router 2

**Nombre de host:** router-IG

[Archivo `startup_config`](./files/Router2_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**

- Comando `show cdp neighbors` :
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-IQ    Gig 0/0          123            R       PT1000      Gig 1/0
Switch       Fas 1/0          123            S       PT3000      Fas 0/1
```

- Comando `show ip arp` :
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.2.1             17  0030.A38B.B5D4  ARPA   FastEthernet1/0
Internet  192.168.2.254           -   00E0.F712.ABC3  ARPA   FastEthernet1/0
Internet  198.51.100.1            17  00E0.B096.E384  ARPA   GigabitEthernet0/0
Internet  198.51.100.2            -   0001.437E.11AB  ARPA   GigabitEthernet0/0
```

- Comando `show ip interface brief` :
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.2    YES NVRAM  up                    up
FastEthernet1/0        192.168.2.254   YES NVRAM  up                    up
```

- Comando `show ip route` :
```text
S    192.168.1.0/24 [1/0] via 198.51.100.1
C    192.168.2.0/24 is directly connected, FastEthernet1/0
S    192.168.3.0/24 [1/0] via 198.51.100.6
                    [1/0] via 198.51.100.14
S    192.168.4.0/24 [1/0] via 198.51.100.10
                    [1/0] via 198.51.100.13
     198.51.100.0/30 is subnetted, 4 subnets
C       198.51.100.0 is directly connected, GigabitEthernet0/0
S       198.51.100.4 [1/0] via 198.51.100.1
S       198.51.100.8 [1/0] via 198.51.100.1
S       198.51.100.12 [1/0] via 198.51.100.6
                      [1/0] via 198.51.100.10
```

- Comando `show ip route summary` :
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           1           144         256
static          3           3           432         768
internal        1                                   1148
Total           5           4           576         2172
```

### Switch 2

**IP de administración:** 192.168.2.253

[Archivo `startup_config`](./files/Switch-LAN-2_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-AI    Fas 0/1          166            R       PT1000      Fas 1/0
```

- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.2.253           -   000D.BD67.21CC  ARPA   Vlan1
```

- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
FastEthernet0/1        unassigned      YES manual up                    up
FastEthernet1/1        unassigned      YES manual up                    up
FastEthernet2/1        unassigned      YES manual down                  down
FastEthernet3/1        unassigned      YES manual down                  down
FastEthernet4/1        unassigned      YES manual down                  down
FastEthernet5/1        unassigned      YES manual down                  down
Vlan1                  192.168.2.253   YES manual up                    up
```
---------------------------------------------
### Router 3

**Nombre de host:** router-IG

[Archivo `startup_config`](./files/Router3_startup-config.txt)


- Comando `show cdp neighbors` :
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-IQ    Gig 0/0          143            R       PT1000      Gig 0/0
router-IGF   Gig 1/0          143            R       PT1000      Gig 0/0
Switch       Fas 3/0          143            S       PT3000      Fas 0/1
```

- Comando `show ip arp` :
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.3.254           -   0050.0F89.1B40  ARPA   FastEthernet3/0
Internet  198.51.100.6            -   0001.63C6.661E  ARPA   GigabitEthernet0/0
Internet  198.51.100.13           -   0001.4224.4E76  ARPA   GigabitEthernet1/0
```

- Comando `show ip interface brief` :
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.6    YES NVRAM  up                    up
GigabitEthernet1/0     198.51.100.13   YES manual up                    up
FastEthernet3/0        192.168.3.254   YES NVRAM  up                    up
```

- Comando `show ip route` :
```text
Gateway of last resort is not set

S    192.168.1.0/24 [1/0] via 198.51.100.5
S    192.168.2.0/24 [1/0] via 198.51.100.2
C    192.168.3.0/24 is directly connected, FastEthernet3/0
S    192.168.4.0/24 [1/0] via 198.51.100.9
                    [1/0] via 198.51.100.14
     198.51.100.0/30 is subnetted, 4 subnets
S       198.51.100.0 [1/0] via 198.51.100.5
C       198.51.100.4 is directly connected, GigabitEthernet0/0
S       198.51.100.8 [1/0] via 198.51.100.5
                     [1/0] via 198.51.100.14
C       198.51.100.12 is directly connected, GigabitEthernet1/0
```

- Comando `show ip route summary` :
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          3           2           360         640
internal        1                                   1148
Total           5           4           576         2172
```

### Switch 3

**IP de administración:** 192.168.3.253

[Archivo `startup_config`](./files/Switch-LAN-3_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-IG    Fas 0/1          160            R       PT1000      Fas 3/0
```

- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.3.253           -   0010.111C.7460  ARPA   Vlan1
```

- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
FastEthernet0/1        unassigned      YES manual up                    up
FastEthernet1/1        unassigned      YES manual up                    up
FastEthernet2/1        unassigned      YES manual down                  down
FastEthernet3/1        unassigned      YES manual down                  down
FastEthernet4/1        unassigned      YES manual down                  down
FastEthernet5/1        unassigned      YES manual down                  down
Vlan1                  192.168.3.253   YES manual up                    up
```
----------------------

### Router 4

**Nombre de host:** router-IGF

[Archivo `startup_config`](./files/Router4_startup-config.txt)


- Comando `show cdp neighbors` :
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
Switch       Fas 3/0          130            S       PT3000      Fas 0/1
router-IG    Gig 0/0          130            R       PT1000      Gig 1/0
router-IQ    Gig 2/0          130            R       PT1000      Gig 2/0
```

- Comando `show ip arp` :
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.4.254           -   00D0.BAD1.8BBA  ARPA   FastEthernet3/0
Internet  198.51.100.10           -   00E0.8F33.3E7B  ARPA   GigabitEthernet2/0
Internet  198.51.100.14           -   0001.420C.E233  ARPA   GigabitEthernet0/0
```

- Comando `show ip interface brief` :
```text
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     198.51.100.14   YES manual up                    up
GigabitEthernet2/0     198.51.100.10   YES manual up                    up
FastEthernet3/0        192.168.4.254   YES manual up                    up
```

- Comando `show ip route` :
```text
Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

S    192.168.1.0/24 [1/0] via 198.51.100.9
                    [1/0] via 198.51.100.6
S    192.168.2.0/24 [1/0] via 198.51.100.2
S    192.168.3.0/24 [1/0] via 198.51.100.5
                    [1/0] via 198.51.100.13
C    192.168.4.0/24 is directly connected, FastEthernet3/0
     198.51.100.0/30 is subnetted, 4 subnets
S       198.51.100.0 [1/0] via 198.51.100.9
S       198.51.100.4 [1/0] via 198.51.100.9
                     [1/0] via 198.51.100.13
C       198.51.100.8 is directly connected, GigabitEthernet2/0
C       198.51.100.12 is directly connected, GigabitEthernet0/0
```

- Comando `show ip route summary` :
```text
IP routing table name is Default-IP-Routing-Table(0)
IP routing table maximum-paths is 16
Route Source    Networks    Subnets     Overhead    Memory (bytes)
connected       1           2           216         384
static          3           2           360         640
internal        1                                   1148
Total           5           4           576         2172
```

### Switch 4

**IP de administración:** 192.168.4.253

[Archivo `startup_config`](./files/Switch-LAN-3_startup-config.txt)

- **Conexión con otros dispositivos `show cdp neighbors`:**
```text
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
router-IGF   Fas 0/1          161            R       PT1000      Fas 3/0
```

- Comando `show ip arp`:
```text
Protocol  Address          Age (min)  Hardware Addr   Type   Interface
Internet  192.168.4.253           -   00D0.BAE2.916A  ARPA   Vlan1
```

- Comando `show ip interface brief`:
```text
Interface              IP-Address      OK? Method Status                Protocol
FastEthernet0/1        unassigned      YES manual up                    up
FastEthernet1/1        unassigned      YES manual up                    up
FastEthernet2/1        unassigned      YES manual down                  down
FastEthernet3/1        unassigned      YES manual down                  down
FastEthernet4/1        unassigned      YES manual down                  down
FastEthernet5/1        unassigned      YES manual down                  down
Vlan1                  192.168.4.253   YES manual up                    up
```

## Pruebas de conectividad (comando `ping`)

### Desde la Laptop 1

#### Laptops

- Laptop2
```text
Pinging 192.168.2.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.2.1: bytes=32 time=11ms TTL=126
Reply from 192.168.2.1: bytes=32 time<1ms TTL=126

Ping statistics for 192.168.2.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 11ms, Average = 5ms
```

- Laptop3
```text
Pinging 192.168.3.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.3.1: bytes=32 time<1ms TTL=126
Reply from 192.168.3.1: bytes=32 time<1ms TTL=126

Ping statistics for 192.168.3.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Laptop4
```text
Request timed out.
Request timed out.
Reply from 192.168.4.1: bytes=32 time=17ms TTL=126
Request timed out.

Ping statistics for 192.168.4.1:
    Packets: Sent = 4, Received = 1, Lost = 3 (75% loss),
Approximate round trip times in milli-seconds:
    Minimum = 17ms, Maximum = 17ms, Average = 17ms
```

#### Routers

- Router2
```text
Pinging 198.51.100.2 with 32 bytes of data:

Reply from 198.51.100.2: bytes=32 time<1ms TTL=254
Reply from 198.51.100.2: bytes=32 time=9ms TTL=254
Reply from 198.51.100.2: bytes=32 time<1ms TTL=254
Reply from 198.51.100.2: bytes=32 time<1ms TTL=254

Ping statistics for 198.51.100.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 9ms, Average = 2ms
```

- Router3
```text
Pinging 198.51.100.13 with 32 bytes of data:

Reply from 192.168.1.254: Destination host unreachable.
Reply from 192.168.1.254: Destination host unreachable.
Reply from 192.168.1.254: Destination host unreachable.
Reply from 192.168.1.254: Destination host unreachable.

Ping statistics for 198.51.100.13:
    Packets: Sent = 4, Received = 0, Lost = 4 (100% loss),
```

- Router4
```text
Pinging 198.51.100.10 with 32 bytes of data:

Request timed out.
Reply from 198.51.100.10: bytes=32 time=1ms TTL=254
Reply from 198.51.100.10: bytes=32 time<1ms TTL=254
Reply from 198.51.100.10: bytes=32 time<1ms TTL=253

Ping statistics for 198.51.100.10:
    Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```

#### Switches

- Switch2
```text
Pinging 192.168.2.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.2.253: bytes=32 time<1ms TTL=253
Reply from 192.168.2.253: bytes=32 time=2ms TTL=253

Ping statistics for 192.168.2.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

- Switch3
```text
Pinging 192.168.3.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.3.253: bytes=32 time<1ms TTL=253
Reply from 192.168.3.253: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.3.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Switch4
```text
Pinging 192.168.4.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.4.253: bytes=32 time=10ms TTL=253
Reply from 192.168.4.253: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.4.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 10ms, Average = 5ms
```

### Desde la Laptop 2

#### Laptops

- Laptop1
```text
Pinging 192.168.1.1 with 32 bytes of data:

Reply from 192.168.1.1: bytes=32 time<1ms TTL=126
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126
Reply from 192.168.1.1: bytes=32 time=8ms TTL=126

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 8ms, Average = 2ms
```

- Laptop3
```text
Pinging 192.168.3.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.3.1: bytes=32 time=1ms TTL=125
Reply from 192.168.3.1: bytes=32 time<1ms TTL=125

Ping statistics for 192.168.3.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```

- Laptop4
```text
Pinging 192.168.4.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.4.1: bytes=32 time=2ms TTL=125
Reply from 192.168.4.1: bytes=32 time<1ms TTL=125

Ping statistics for 192.168.4.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

#### Routers

- Router1
```text
Pinging 198.51.100.1 with 32 bytes of data:

Reply from 198.51.100.1: bytes=32 time<1ms TTL=254
Reply from 198.51.100.1: bytes=32 time<1ms TTL=254
Reply from 198.51.100.1: bytes=32 time<1ms TTL=254
Reply from 198.51.100.1: bytes=32 time<1ms TTL=254

Ping statistics for 198.51.100.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Router3
```text
Pinging 198.51.100.6 with 32 bytes of data:

Reply from 198.51.100.6: bytes=32 time<1ms TTL=253
Reply from 198.51.100.6: bytes=32 time<1ms TTL=253
Reply from 198.51.100.6: bytes=32 time<1ms TTL=253
Reply from 198.51.100.6: bytes=32 time=11ms TTL=253

Ping statistics for 198.51.100.6:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 11ms, Average = 2ms
```

- Router4
```text
Pinging 198.51.100.10 with 32 bytes of data:

Reply from 198.51.100.10: bytes=32 time<1ms TTL=253
Reply from 198.51.100.10: bytes=32 time<1ms TTL=253
Reply from 198.51.100.10: bytes=32 time<1ms TTL=253
Reply from 198.51.100.10: bytes=32 time<1ms TTL=253

Ping statistics for 198.51.100.10:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

#### Switches

- Switch1
```text
Pinging 192.168.1.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.1.253: bytes=32 time=2ms TTL=253
Reply from 192.168.1.253: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.1.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

- Switch3
```text
Pinging 192.168.3.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.3.253: bytes=32 time<1ms TTL=252
Reply from 192.168.3.253: bytes=32 time<1ms TTL=252

Ping statistics for 192.168.3.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Switch4
```text
Pinging 192.168.4.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.4.253: bytes=32 time<1ms TTL=252
Reply from 192.168.4.253: bytes=32 time<1ms TTL=252

Ping statistics for 192.168.4.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

### Desde la Laptop 3

#### Laptops

- Laptop1
```text
Pinging 192.168.1.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Laptop2
```text
Pinging 192.168.2.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.2.1: bytes=32 time<1ms TTL=125
Reply from 192.168.2.1: bytes=32 time<1ms TTL=125

Ping statistics for 192.168.2.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Laptop4
```text
Pinging 192.168.4.1 with 32 bytes of data:

Request timed out.
Request timed out.
Request timed out.
Reply from 192.168.4.1: bytes=32 time<1ms TTL=126

Ping statistics for 192.168.4.1:
    Packets: Sent = 4, Received = 1, Lost = 3 (75% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

#### Routers

- Router1
```text
Pinging 198.51.100.5 with 32 bytes of data:

Reply from 198.51.100.5: bytes=32 time<1ms TTL=254
Reply from 198.51.100.5: bytes=32 time<1ms TTL=254
Reply from 198.51.100.5: bytes=32 time<1ms TTL=254
Reply from 198.51.100.5: bytes=32 time<1ms TTL=254

Ping statistics for 198.51.100.5:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Router2
```text
Pinging 198.51.100.2 with 32 bytes of data:

Reply from 198.51.100.2: bytes=32 time<1ms TTL=253
Reply from 198.51.100.2: bytes=32 time<1ms TTL=253
Reply from 198.51.100.2: bytes=32 time<1ms TTL=253
Reply from 198.51.100.2: bytes=32 time<1ms TTL=253

Ping statistics for 198.51.100.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Router4
```text
Pinging 198.51.100.4 with 32 bytes of data:

Reply from 192.168.3.254: bytes=32 time<1ms TTL=255
Reply from 192.168.3.254: bytes=32 time<1ms TTL=255
Reply from 192.168.3.254: bytes=32 time<1ms TTL=255
Reply from 192.168.3.254: bytes=32 time<1ms TTL=255

Ping statistics for 198.51.100.4:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

#### Switches

- Switch1
```text
Pinging 192.168.1.254 with 32 bytes of data:

Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254

Ping statistics for 192.168.1.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

- Switch2
```text
Pinging 192.168.2.254 with 32 bytes of data:

Reply from 192.168.2.254: bytes=32 time=5ms TTL=253
Reply from 192.168.2.254: bytes=32 time<1ms TTL=253
Reply from 192.168.2.254: bytes=32 time=1ms TTL=253
Reply from 192.168.2.254: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.2.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 5ms, Average = 1ms
```

- Switch4
```text
Pinging 192.168.4.254 with 32 bytes of data:

Reply from 192.168.4.254: bytes=32 time=11ms TTL=254
Reply from 192.168.4.254: bytes=32 time<1ms TTL=253
Reply from 192.168.4.254: bytes=32 time<1ms TTL=254
Reply from 192.168.4.254: bytes=32 time=7ms TTL=254

Ping statistics for 192.168.4.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 11ms, Average = 4ms
```

### Desde la Laptop 4

#### Laptops

- Laptop1
```text
Pinging 192.168.1.1 with 32 bytes of data:

Request timed out.
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126
Reply from 192.168.1.1: bytes=32 time=1ms TTL=126
Reply from 192.168.1.1: bytes=32 time<1ms TTL=126

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```
- Laptop2
```text
Pinging 192.168.2.1 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.2.1: bytes=32 time<1ms TTL=125
Reply from 192.168.2.1: bytes=32 time=1ms TTL=125

Ping statistics for 192.168.2.1:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```

- Laptop3
```text
Pinging 192.168.3.1 with 32 bytes of data:

Request timed out.
Reply from 192.168.3.1: bytes=32 time<1ms TTL=126
Reply from 192.168.3.1: bytes=32 time<1ms TTL=126
Reply from 192.168.3.1: bytes=32 time=1ms TTL=125

Ping statistics for 192.168.3.1:
    Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```
#### Routers
- Router1
```text
Pinging 192.168.1.254 with 32 bytes of data:

Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254
Reply from 192.168.1.254: bytes=32 time<1ms TTL=254

Ping statistics for 192.168.1.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```
- Router2
```text
Pinging 192.168.2.254 with 32 bytes of data:

Reply from 192.168.2.254: bytes=32 time=22ms TTL=253
Reply from 192.168.2.254: bytes=32 time<1ms TTL=253
Reply from 192.168.2.254: bytes=32 time<1ms TTL=253
Reply from 192.168.2.254: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.2.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 22ms, Average = 5ms

```
- Router3
```text
Pinging 192.168.3.254 with 32 bytes of data:

Reply from 192.168.3.254: bytes=32 time<1ms TTL=254
Reply from 192.168.3.254: bytes=32 time<1ms TTL=253
Reply from 192.168.3.254: bytes=32 time<1ms TTL=254
Reply from 192.168.3.254: bytes=32 time<1ms TTL=254

Ping statistics for 192.168.3.254:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```
#### Switches
- Switch1
```text
Pinging 192.168.1.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.1.253: bytes=32 time<1ms TTL=253
Reply from 192.168.1.253: bytes=32 time<1ms TTL=253

Ping statistics for 192.168.1.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```
- Switch2
```text
Pinging 192.168.2.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.2.253: bytes=32 time<1ms TTL=252
Reply from 192.168.2.253: bytes=32 time<1ms TTL=252

Ping statistics for 192.168.2.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```
- Switch3
```text
Pinging 192.168.3.253 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 192.168.3.253: bytes=32 time<1ms TTL=253
Reply from 192.168.3.253: bytes=32 time=8ms TTL=253

Ping statistics for 192.168.3.253:
    Packets: Sent = 4, Received = 2, Lost = 2 (50% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 8ms, Average = 4ms
```
