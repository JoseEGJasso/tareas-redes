# Práctica 1 - Equipo-DMD-GJJE-LGC-MEB-OSMA

## Integrantes:

- Dozal Magnani Diego - 316032708 - @diegodmag
- González Jasso José Eduardo - 316093837 - @JoseEGJasso
- Lorenzo Guerrero Celeste - 316162027 - @CelesteLG
- Martinez Enriquez Bruno - 316126597 - @BrunoMartz
- Ordóñez Silis Miguel Ángel - 417034052 - @SirMaik

## Imágenes del edificio
**Edificio elegido:** Biblioteca "Enrique Rivero Borrell" del Anexo de Ingeniería

| ![](img/biblioteca-1.jpg)|
|:------------------------:|
| Anonimo.Biblioteca «Mtro. Enrique Rivero Borrell». (2009, 2 enero).Wikipedia. https://es.m.wikipedia.org/wiki/Archivo:Biblioteca_%22Mtro._Enrique_Rivero_Borrell%22.jpg  |

| ![](img/biblioteca-2.jpg)|
|:------------------------:|
| Paredes, R. (2013, 6 mayo). Biblioteca Enrique Rivero Borrell. Foursquare. https://fastly.4sqi.net/img/general/width960/11895194_zPIXsX3OL1LpaPX4bEdLE3TiUTN-KPEFizqmRCm8fu4.jpg|

## Explicación de la topología de red utilizada.
| ![](img/topologia.png)|
|:------------------------:|
| Topología de la red  |

Para esta práctica elegimos la **Biblioteca "Enrique Rivero Borrell"** del Anexo de Ingeniería que cuenta con un piso y la planta baja. Colocamos un _switch de distribución_ y dos _switch de acceso_ por cada piso del edifcio. La planta baja cuenta con un área de estudio por lo que pusimos dispositivos inalámbricos, las PCs conectadas alámbricamente representan las computadoras del personal de la bilioteca.
## Tablas.

### Planta baja
| Equipo | Nombre de host | IP de Administración   | Switches |
|--------|----------------|------------------------|--------|
| Switch-Core | switch-core | 10.0.0.1 | - |
| Switch-Distribución-PB | switch-dist-PB | 10.0.0.2 | < switch-core > switch-access-PB-1,switch-access-PB-2 |
| Switch-Access-PB-1 | switch-access-PB-1 | 10.0.0.4 | < switch-dist-PB  |
| Switch-Access-PB-2 | switch-access-PB-2 | 10.0.0.5 | < switch-dist-PB  |

### Piso 1
| Equipo | Nombre de host | IP de Administración   | Switches |
|--------|----------------|------------------------|--------|
| Switch-Distribución-P1 | switch-dist-P1 | 10.0.0.3 | < switch-core > switch-access-PB-1,switch-access-PB-2 |
| Switch-Access-P1-1 | switch-access-P1-1 | 10.0.0.6 | < switch-dist-P1  |
| Switch-Access-PB-2 | switch-access-PB-2 | 10.0.0.7 | < switch-dist-P1  |

## Pruebas de conectividad
| ![](img/test-1.png)|
|:------------------------:|
| Conectividad de un equipo a su switch de acceso |

| ![](img/test-2.png)|
|:------------------------:|
| Conectividad de un equipo a otro del mismo piso que está en otro switch de acceso (piso 1)|

| ![](img/test-3.png)|
|:------------------------:|
| Conectividad de un equipo a otro que están en pisos distintos (piso 1 a planta baja) |

| ![](img/test-3-1.png)|
|:------------------------:|
| Conectividad de un equipo a otro que están en pisos distintos (planta baja a piso 1) |

| ![](img/test-4.png)|
|:------------------------:|
| Conectividad desde un equipo al switch de distribución de su mismo piso (planta baja)|

| ![](img/test-5.png)|
|:------------------------:|
| Conectividad desde un equipo al switch de distribución de otro piso (planta baja a piso 1) |

| ![](img/test-6.png)|
|:------------------------:|
| Conectividad desde un equipo al switch core  |
