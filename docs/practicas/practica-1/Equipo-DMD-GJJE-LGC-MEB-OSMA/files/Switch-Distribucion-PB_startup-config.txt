!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname switch-dist-PB
!
enable secret 5 $1$mERr$YsXqIF8306MddLsesE9MP/
!
!
!
ip ssh version 2
ip domain-name cisco.com
!
username netadmin privilege 1 password 0 netadmin
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface FastEthernet2/1
!
interface FastEthernet3/1
!
interface FastEthernet4/1
!
interface FastEthernet5/1
!
interface FastEthernet6/1
!
interface GigabitEthernet7/1
!
interface Vlan1
 ip address 10.0.0.2 255.255.255.0
!
banner motd E
WARNING
If you don't have authorization, don't try log in E
!
!
!
line con 0
 password cisco
 logging synchronous
 login
!
line vty 0 4
 password sanjose
 login local
 transport input ssh
line vty 5 15
 password sanjose
 login local
 transport input ssh
!
!
!
!
end
