# Debian

## Verificación de resolución DNS

Salida comandos `dig`:

 - Resolución con el DNS local `@127.0.0.1`:
   - `dig example.com. @127.0.0.1`:
     ```
       ; <<>> DiG 9.16.27-Debian <<>> example.com. @127.0.0.1
      ;; global options: +cmd
      ;; Got answer:
      ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18778
      ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

      ;; OPT PSEUDOSECTION:
      ; EDNS: version: 0, flags:; udp: 512
      ;; QUESTION SECTION:
      ;example.com.                   IN      A

      ;; ANSWER SECTION:
      example.com.            85608   IN      A       93.184.216.34

      ;; Query time: 28 msec
      ;; SERVER: 127.0.0.1#53(127.0.0.1)
      ;; WHEN: Wed Oct 26 20:04:07 CDT 2022
      ;; MSG SIZE  rcvd: 56
     ```
   - `dig gateway.local. @127.0.0.1`:
     ```
       ; <<>> DiG 9.16.27-Debian <<>> gateway.local. @127.0.0.1
      ;; global options: +cmd
      ;; Got answer:
      ;; WARNING: .local is reserved for Multicast DNS
      ;; You are currently testing what happens when an mDNS query is leaked to DNS
      ;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 44433
      ;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

      ;; OPT PSEUDOSECTION:
      ; EDNS: version: 0, flags:; udp: 512
      ;; QUESTION SECTION:
      ;gateway.local.                 IN      A

      ;; AUTHORITY SECTION:
      .                       10800   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2022102602 1800 900 604800 86400

      ;; Query time: 120 msec
      ;; SERVER: 127.0.0.1#53(127.0.0.1)
      ;; WHEN: Wed Oct 26 20:05:34 CDT 2022
      ;; MSG SIZE  rcvd: 117
     ```

- Resolución con `@9.9.9.9`:
   - `dig example.com. @9.9.9.9`:
     ```
       ; <<>> DiG 9.16.27-Debian <<>> example.com. @9.9.9.9
      ;; global options: +cmd
      ;; Got answer:
      ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 6889
      ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

      ;; OPT PSEUDOSECTION:
      ; EDNS: version: 0, flags:; udp: 512
      ;; QUESTION SECTION:
      ;example.com.                   IN      A

      ;; ANSWER SECTION:
      example.com.            43200   IN      A       93.184.216.34

      ;; Query time: 228 msec
      ;; SERVER: 9.9.9.9#53(9.9.9.9)
      ;; WHEN: Wed Oct 26 20:07:35 CDT 2022
      ;; MSG SIZE  rcvd: 56
     ```
   - `dig gateway.local. @9.9.9.9`:
     ```
       ; <<>> DiG 9.16.27-Debian <<>> gateway.local. @9.9.9.9
      ;; global options: +cmd
      ;; Got answer:
      ;; WARNING: .local is reserved for Multicast DNS
      ;; You are currently testing what happens when an mDNS query is leaked to DNS
      ;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 24265
      ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

      ;; OPT PSEUDOSECTION:
      ; EDNS: version: 0, flags:; udp: 1232
      ;; QUESTION SECTION:
      ;gateway.local.                 IN      A

      ;; AUTHORITY SECTION:
      .                       814     IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2022102602 1800 900 604800 86400

      ;; Query time: 168 msec
      ;; SERVER: 9.9.9.9#53(9.9.9.9)
      ;; WHEN: Wed Oct 26 20:09:21 CDT 2022
      ;; MSG SIZE  rcvd: 117
     ```

## Verificación de interfaces de red

- `ip a`
  ```
  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
         valid_lft forever preferred_lft forever
  2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:e4:d8:e7 brd ff:ff:ff:ff:ff:ff
      altname enp0s3
      inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic eth0
         valid_lft 85711sec preferred_lft 85711sec
      inet6 fe80::a00:27ff:fee4:d8e7/64 scope link
         valid_lft forever preferred_lft forever
  3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:07:d7:3e brd ff:ff:ff:ff:ff:ff
      altname enp0s8
      inet 192.168.56.254/24 brd 192.168.56.255 scope global eth1
         valid_lft forever preferred_lft forever
      inet6 fe80::a00:27ff:fe07:d73e/64 scope link
         valid_lft forever preferred_lft forever
  ```
