# Alpine

## Conectividad local
```
alpine:~# ping -c 4 192.168.56.254
PING 192.168.56.254 (192.168.56.254): 56 data bytes
64 bytes from 192.168.56.254: seq=0 ttl=64 time=0.402 ms
64 bytes from 192.168.56.254: seq=1 ttl=64 time=0.375 ms
64 bytes from 192.168.56.254: seq=2 ttl=64 time=0.537 ms
64 bytes from 192.168.56.254: seq=3 ttl=64 time=0.519 ms

--- 192.168.56.254 ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max = 0.375/0.458/0.537 ms

# Conectividad a la máquina virtual de CentOS
alpine:~# ping -c 4 192.168.56.102
PING 192.168.56.102 (192.168.56.102): 56 data bytes
64 bytes from 192.168.56.102: seq=0 ttl=64 time=0.390 ms
64 bytes from 192.168.56.102: seq=1 ttl=64 time=0.545 ms
64 bytes from 192.168.56.102: seq=2 ttl=64 time=0.461 ms
64 bytes from 192.168.56.102: seq=3 ttl=64 time=0.513 ms

--- 192.168.56.102 ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max = 0.390/0.477/0.545 ms

alpine:~# ping -c 4 dns.local
PING dns.local (192.168.56.254): 56 data bytes
64 bytes from 192.168.56.254: seq=0 ttl=64 time=0.320 ms
64 bytes from 192.168.56.254: seq=1 ttl=64 time=0.573 ms
64 bytes from 192.168.56.254: seq=2 ttl=64 time=0.495 ms
64 bytes from 192.168.56.254: seq=3 ttl=64 time=0.926 ms

--- dns.local ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max = 0.320/0.578/0.926 ms
```

## Conectividad Externa
```
alpine:~# ping -c 4 1.1.1.1
PING 1.1.1.1 (1.1.1.1): 56 data bytes
64 bytes from 1.1.1.1: seq=0 ttl=61 time=20.209 ms
64 bytes from 1.1.1.1: seq=1 ttl=61 time=22.817 ms
64 bytes from 1.1.1.1: seq=2 ttl=61 time=6.678 ms
64 bytes from 1.1.1.1: seq=3 ttl=61 time=7.977 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max = 6.678/14.420/22.817 ms

alpine:~# ping -c 4 example.com.
PING example.com. (93.184.216.34): 56 data bytes
64 bytes from 93.184.216.34: seq=0 ttl=61 time=12.112 ms
64 bytes from 93.184.216.34: seq=1 ttl=61 time=22.773 ms
64 bytes from 93.184.216.34: seq=2 ttl=61 time=55.199 ms
64 bytes from 93.184.216.34: seq=3 ttl=61 time=15.595 ms

--- example.com. ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max = 12.112/26.419/55.199 ms

alpine:~# wget -qcO - http://example.com/ | egrep '</?title>'
    <title>Example Domain</title>

```
