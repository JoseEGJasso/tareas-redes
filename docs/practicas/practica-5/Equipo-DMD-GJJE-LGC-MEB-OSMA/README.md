# Práctica 5 - Equipo-DMD-GJJE-LGC-MEB-OSMA

## Integrantes:

- Dozal Magnani Diego - 316032708 - @diegodmag
- González Jasso José Eduardo - 316093837 - @JoseEGJasso
- Lorenzo Guerrero Celeste - 316162027 - @CelesteLG
- Martinez Enriquez Bruno - 316126597 - @BrunoMartz
- Ordóñez Silis Miguel Ángel - 417034052 - @SirMaik

---

## General

### Explicación de la topología de red utilizada

La topología de red utilizada es de malla en donde la máquina virtual Debian funciona como router para proporcionar de servicio de red (a través de la host-only) a las máquinas virtuales CentOs y Alpine, las cuales actúan como clientes DHCP, donde la MV Debian funge como servidor DHCP que les proporciona a ambas máquinas una ip diferente.
La MV Debian, a su vez, hace uso de un router NAT, el cual le da el acceso a internet.

### Procedimiento de configuración de NAT, forwarder DNS y servidor DHCP

#### NAT

En iptables, habilitamos las reglas en la tabla NAT e indicamos que el tráfico de entrada va por host-only y de salida por la NAT.
Además se debe configurar una red tipo NAT en la máquina que se utilizará como router (la de Debian en este caso).

#### Forwarder DNS

El proceso consta de asignar una dirección IP a un nombre DNS y especificar la red donde recibirá las peticiones de los clientes, también se configura para enviar las consultas DNS a los servidores externos. Por último, se realiza una configuración para enciar las consultas locales de DNS a la instancia de `dnsmasq`.

#### Servidor DHCP

Se configura un nombre de dominio y el gateway, así como especificar el rango de direcciones asignables y especificar la interfaz de red en donde se dará el servicio de DHCP.

Esto está detallado en la sección de **Debian**.

### Procedimiento para reservar una dirección IP en el servidor DHCP

- **Discovery**: El cliente solicita una IP al servidor DHCP.
- **Offer**: El servidor DHCP le ofrece una dirección al solicitante.
- **Request**: El cliente decide si utilizar la dirección ofrecida. Existen casos donde existen varios servidores DHCP y éstos le ofrecen una IP al cliente, en donde éste decide con cuál quedarse y notifica su decisión al servidor (o los servidores).
- **Acknowledge**: Por último, el servidor le hace confirma al cliente que la IP solicitada ya le pertenece o si ya no está disponible.

### Explicar el contenido del archivo leases del servidor y cliente DHCP

El documento guarda la lista de leases (traducido como arrendamientos) que otorga el servidor DHCP a los clientes.
En cada entrada viene la IP otorgada al cliente, donde destacamos la fecha y hora de inicio y fin del préstamo de dirección, el estado, la MAC y el hostname del cliente.

### Explicar detalladamente cómo es que el cliente obtiene una dirección IP del servidor DHCP

Como se explicó anteriormente en el procedimiento de reservación de IP, el cliente envía una petición tipo broadcast con dirección IP de origen 0.0.0.0 y destino 255.255.255.255; el servidor le enviará una respuesta de ofrecimiento unicast donde la IP de origen es la del servidor y la destino 255.255.255.255, en donde encontramos una dirección IP privada que contiene la dirección MAC del cliente; el cliente responde solicitando dicha IP al servidor que se la ofreció (la comunicación es de tipo broadcast porque aún no tiene una dirección IP privada); finalmente, el servidor le confirma que dicha IP ya le pertenece, donde también recibe la información de todos los equipos de la red local con el fin de evitar conflictos.

### Qué ventajas y desventajas existen al tener un forwarder DNS en la red local

Ventajas:
- Funciona como un router en caso de recibir un paquete que no esté destinado para el forwarder (lo acepta y lo redirige)
- Guarda en la memoria caché las consultas que no se han habían hecho anteriormente, por lo cual una consulta recurrente tendrá una respuesta rápida.
- Se puede configurar un firewall (el procedimiento hecho con iptables) para dirigir el tráfico de red.

Desventajas:
- Si la máquina que funge como forwarder DNS falla o es apagada, todos las otras máquinas cuya conexión dependía del forwarder se quedan sin conexión.

### Explicar detalladamente cómo es que una petición se envía desde el cliente hasta su destino fuera de la red host-only

La petición del cliente pasa por la red host-only a través de la interfaz host-only previamente configurada, la cual después se dirige al router a través de la interfaz de red host-only (el cual en este caso es la MV Debian). Aquí es cuando la petición pasa a través de la otra interfaz de red tipo NAT, la cual, según la topología de red, es la que tiene la conexión a internet.

### Conclusiones sobre las capturas de tráfico de red

#### ¿Hay alguna diferencia en el tráfico de las capturas de red?
 - Entre los dos PCAPs de debian cambian las IPs, lo cual tiene sentido ya que se encuentran en dos redes distintas.
 - Entre los dos PCAPs de debian cambian las IPs. En la de eth0 se usa la IP `10.0.2.15` para todo el tráfico mientras que en la eth1 aparecen las IPs particulares de cada uno de los equipos en la red virtualbox0. Las IPs de los destinatarios se mantienen igual.
 - El tráfico de la única interfaz de CentOS es muy parecido a la de la interfaz eth1 de Debian. Esto tiene sentido ya que se encuentran en la misma red y gran parte del tráfico involucra a estos dos equipos.
 - El tráfico capturado en la interfaz eth0 de debian es mucho menor. Al llevar a cabo el ARP sólo se encuentra a otro dispositivo que es el del gateway de esta red.
 - Entre las dos interfaces de debian cambian los destinatarios de las peticiones DNS. En la eth1 el destinatario es la máquina debian misma mientras que en la eth0 los destinatarios son las IPs que configuramos (1.1.1.1 ...).
 - Las capturas de la interfaz eth0 de Debian no tiene tráfico DHCP, lo cual tiene sentido ya que las renovaciones se llevan a cabo en la otra red.

#### ¿Qué parte de tráfico de DHCP es *broadcast* y *unicast*?
- **broadcast**: Todos el tráfico **discover** es broadcast. Algunas de las peticiones **request** son broadcast ya que no se sabe si hay servidores dhcp y en el caso de que hayan pueden haber múltiples de esto.
- **unicast**:  Todo el tráfico **ack** (acknowledge) y **offer** es unicast. Algunas de las peticiones **request** son unicast cuando el cliente ya sabe que cierta IP corresponde a un servidor DHCP.

#### ¿Cómo es que funciona ARP?
Se hace un broadcast preguntando si algún equipo está usando una IP en particular. En el caso de que algún equipo esté usando dicha IP responde directamente diciéndo que la está usando y especificando su dirección MAC.


#### ¿Cómo es que llega una petición y respuesta de `PING` desde la máquina **CentOS** hasta un servidor en Internet?


- `$ ping -c 4 8.8.8.8`: La petición lleva al servidor DNS de la máquina debian. Esta máquina hace peticiones DNS a cada uno de los servidores que configuramos (1.1.1.1, 8.8.8.8 y 9.9.9.9).
  La petición continúa por la máquina física, red local física.... Las respuestas, si es que existen (en nuestro caso 9.9.9.9 falló), llegan a Debian y las esta máquina las regresa a CentOS.

- `$ ping6 -c 6 example.com.`: Estas peticiones no llegan ya que el gateway y el DNS en debian están configurados sólo para IPV4.



## Configuración y pruebas de conectividad

### [Debian](./files/Debian/README.md)

### [CentOs](./files/CentOS/README.md)

### [Alpine](./files/Alpine/README.md)
