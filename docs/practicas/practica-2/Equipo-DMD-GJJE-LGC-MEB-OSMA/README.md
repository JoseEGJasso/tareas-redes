# Práctica 2 - Equipo-DMD-GJJE-LGC-MEB-OSMA

## Integrantes:

- Dozal Magnani Diego - 316032708 - @diegodmag
- González Jasso José Eduardo - 316093837 - @JoseEGJasso
- Lorenzo Guerrero Celeste - 316162027 - @CelesteLG
- Martinez Enriquez Bruno - 316126597 - @BrunoMartz
- Ordóñez Silis Miguel Ángel - 417034052 - @SirMaik

---
## Explicación de la topología

| ![](img/topologia.png)|
|:------------------------:|
| Topología de la red  |

Las VLAN definidas es esta red las hicimos con el objetivo de simular los posibles grupos de dispositivos dentro de la biblioteca "Enrique Rivero Borrell" como computadoras de los encargados, de los administradores, clientes conectados inalábricamente, etc.

## Tabla de equipos por vLAN

| vLAN  |           Equipos           |
|:-----:|:----------------------------|
| 1 (192.168.1.X/24) | <ul> <li>Smartphone(1) (192.168.1.8/24)</li> <li>Smartphone1 (192.168.1.9/24)</li> <li>Smartphone0 (192.168.1.10/24)</li> <li>Laptop1 (192.168.1.11/24)</li> <li>Laptop0 (192.168.1.12/24)</li> <li>Tablet (192.168.1.13/24)</li> </ul>|
| 2 (192.168.2.X/24) | <ul> <li>PC-1-PB (192.168.2.8/24)</li>  <li>PC-P1-1 (192.168.2.9/24)</li> <li>PC-P1 (192.168.2.10/24)</li> </ul>|
| 3 (192.168.3.X/24) | <ul> <li>PC-2-PB (192.168.3.8/24)</li> <li>PC-3-PB (192.168.3.9/24)</li> <li>Server-P1 (192.168.3.10/24)</li> <li>PC-P1-2 (192.168.3.11/24)</li> <li>PC-P1-3 (192.168.3.12/24)</li>  </ul>|
| 4 (192.168.4.X/24) | <ul> <li>PC-P1-5 (192.168.4.8/24)</li> <li>PC-4-PB (192.168.4.9/24)</li> <li>PC-P1-4 (192.168.4.10/24)</li> <li>Printer (192.168.4.11/24)</li> </ul>|
| 5 (192.168.5.X/24) | <ul> <li> switch-access-P1-1 </li> <li> switch-access-P1-2 </li> <li> switch-access-PB-1 </li> <li> switch-access-PB-1 </li> <li> switch-distr-P1 </li> <li> switch-distr-PB </li> <li> switch-core </li> </ul>|

## Tabla de conexión de cada switch (comando `show cdp neighbors`)
| Equipo | Lista de direcciones IP |
|--------|----------------|
| switch-core | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-dist-PB</td><td>Fas 0/2</td><td>128</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr><tr><td>switch-dist-P1</td><td>Fas 0/1</td><td>128</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr></table> |
| switch-dist-PB | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-access-PB-1</td><td>Gig 1/1</td><td>136</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr><tr><td>switch-core</td><td>Gig 0/1</td><td>136</td><td></td><td>3560</td><td>Fas 0/2</td></tr><tr><td>switch-access-PB-2</td><td>Gig 7/1</td><td>136</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr></table>|
| switch-dist-P1 | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-access-P1-2</td><td>Gig 7/1</td><td>150</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr><tr><td>switch-core</td><td>Gig 0/1</td><td>150</td><td></td><td>3560</td><td>Fas 0/1</td></tr><tr><td>switch-access-P1-1</td><td>Gig 1/1</td><td>150</td><td>S</td><td>PT3000</td><td>Gig 0/1</td></tr></table>|
| switch-access-PB-1 | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-dist-PB</td><td>Gig 0/1</td><td>160</td><td>S</td><td>PT3000</td><td>Gig 1/1</td></tr></table>|
| switch-access-PB-2 | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-dist-PB</td><td>Gig 0/1</td><td>147</td><td>S</td><td>PT3000</td><td>Gig 7/1</td></tr></table>|
| switch-access-P1-1 | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-dist-P1</td><td>Gig 0/1</td><td>139</td><td>S</td><td>PT3000</td><td>Gig 1/1</td></tr></table>|
| switch-access-P1-2 | <table><tr><td>Device ID</td><td>Local Intrfce</td><td>Holdtme</td><td>Capability</td><td>Platform</td><td>Port ID</td></tr><tr><td>switch-dist-P1</td><td>Gig 0/1</td><td>144</td><td>S</td><td>PT3000</td><td>Gig 7/1</td></tr></table>|
## Lista de direcciones IP de cada equipo (comando `show ip interface brief`)
| Equipo | Lista de direcciones IP |
|--------|----------------|
| switch-core | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>FastEthernet0/1</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>up</td><td>up</td></tr><tr><td>FastEthernet0/2</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>up</td><td>up</td></tr><tr><td>FastEthernet0/3</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/4</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/5</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/6</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/7</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/8</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/9</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/10</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/11</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/12</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/13</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/14</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/15</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/16</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/17</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/18</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/19</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/20</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/21</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/22</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/23</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>FastEthernet0/24</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>GigabitEthernet0/2</td><td>unassigned</td><td>YES</td><td>NVRAM</td><td>down</td><td>down</td></tr><tr><td>Vlan1</td><td>192.168.1.1</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.1</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.1</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.1</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.1</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-dist-PB | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>GigabitEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>GigabitEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan1</td><td>192.168.1.2</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.2</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.2</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.2</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.2</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-dist-P1 | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>GigabitEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>GigabitEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan1</td><td>192.168.1.3</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.3</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.3</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.3</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.3</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-access-PB-1 | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet8/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet9/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>Vlan1</td><td>192.168.1.4</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.4</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.4</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.4</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.4</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-access-PB-2 | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet8/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet9/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>Vlan1</td><td>192.168.1.5</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.5</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.5</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.5</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.5</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-access-P1-1 | <table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet8/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet9/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>Vlan1</td><td>192.168.1.6</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.6</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.6</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.6</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.6</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |
| switch-access-P1-2 |<table><tr><td>Interface</td><td>IP-Address</td><td>OK?</td><td>Method</td><td>Status</td><td>Protocol</td></tr><tr><td>GigabitEthernet0/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet1/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet2/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet3/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>FastEthernet4/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet5/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet6/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet7/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet8/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>FastEthernet9/1</td><td>unassigned</td><td>YES</td><td>manual</td><td>down</td><td>down</td></tr><tr><td>Vlan1</td><td>192.168.1.7</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan2</td><td>192.168.2.7</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan3</td><td>192.168.3.7</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan4</td><td>192.168.4.7</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr><tr><td>Vlan5</td><td>192.168.5.7</td><td>YES</td><td>manual</td><td>up</td><td>up</td></tr></table> |

## Tabla de ruteo del switch multicapa (comando `show ip route`)
        switch-core>show ip route
        Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
        D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
        N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
        E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
        i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
        * - candidate default, U - per-user static route, o - ODR
        P - periodic downloaded static route

        Gateway of last resort is not set

        C    192.168.1.0/24 is directly connected, Vlan1
        C    192.168.2.0/24 is directly connected, Vlan2
        C    192.168.3.0/24 is directly connected, Vlan3
        C    192.168.4.0/24 is directly connected, Vlan4
        C    192.168.5.0/24 is directly connected, Vlan5


## Pruebas de conectividad

* Conexión entre el equipo Smartphone0 (192.168.1.10) y Smartphone2 (192.168.1.13) de la VLAN 1
```text
C:\>ping 192.168.1.13

Pinging 192.168.1.13 with 32 bytes of data:

Reply from 192.168.1.13: bytes=32 time=116ms TTL=128
Reply from 192.168.1.13: bytes=32 time=62ms TTL=128
Reply from 192.168.1.13: bytes=32 time=38ms TTL=128
Reply from 192.168.1.13: bytes=32 time=35ms TTL=128

Ping statistics for 192.168.1.13:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 35ms, Maximum = 116ms, Average = 62ms
```

* Conexión entre el equipo PC-P1-1 (192.168.2.9) y PC-1-PB (192.168.2.10) de la VLAN 2
```text
C:\> ping 192.168.2.10

Pinging 192.168.2.10 with 32 bytes of data:

Reply from 192.168.2.10: bytes=32 time=22ms TTL=128
Reply from 192.168.2.10: bytes=32 time<1ms TTL=128
Reply from 192.168.2.10: bytes=32 time<1ms TTL=128
Reply from 192.168.2.10: bytes=32 time<1ms TTL=128

Ping statistics for 192.168.2.10:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 22ms, Average = 5ms
```

* Conexión entre el equipo Server-P1 (192.168.3.11) y PC-2-PB (192.168.3.8) de la VLAN 3
```text
C:\> ping 192.168.3.8

Pinging 192.168.3.8 with 32 bytes of data:

Reply from 192.168.3.8: bytes=32 time<1ms TTL=128
Reply from 192.168.3.8: bytes=32 time<1ms TTL=128
Reply from 192.168.3.8: bytes=32 time<1ms TTL=128
Reply from 192.168.3.8: bytes=32 time<1ms TTL=128

Ping statistics for 192.168.3.8:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

* Conexión entre el equipo PC-P1-5 (192.168.4.9) y Printer (192.168.4.8) de la VLAN 4
```text
C:\> ping 192.168.4.8

Pinging 192.168.4.8 with 32 bytes of data:

Reply from 192.168.4.8: bytes=32 time<1ms TTL=128
Reply from 192.168.4.8: bytes=32 time<1ms TTL=128
Reply from 192.168.4.8: bytes=32 time<1ms TTL=128
Reply from 192.168.4.8: bytes=32 time=10ms TTL=128

Ping statistics for 192.168.4.8:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 10ms, Average = 2ms
```

* Conexión entre el switch-core (192.168.5.1) y switch-access-P1-2 (192.168.5.7) de la VLAN 5
```text
switch-core>ping 192.168.5.7

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 192.168.5.7, timeout is 2 seconds:
..!!!
Success rate is 60 percent (3/5), round-trip min/avg/max = 0/0/1 ms

switch-core>
```

* Conexión entre el equipo PC-P1-2 (192.168.3.8) de la VLAN 3 y Laptop1 (192.168.1.12) de la VLAN 1
```text
C:\> ping 192.168.1.12

Pinging 192.168.1.12 with 32 bytes of data:

Request timed out.
Reply from 192.168.1.12: bytes=32 time=32ms TTL=127
Reply from 192.168.1.12: bytes=32 time=36ms TTL=127
Reply from 192.168.1.12: bytes=32 time=25ms TTL=127

Ping statistics for 192.168.1.12:
    Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),
Approximate round trip times in milli-seconds:
    Minimum = 25ms, Maximum = 36ms, Average = 31ms
```
