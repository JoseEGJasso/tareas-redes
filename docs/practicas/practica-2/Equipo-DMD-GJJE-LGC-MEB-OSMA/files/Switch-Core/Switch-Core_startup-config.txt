!
version 12.2(37)SE1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname switch-core
!
!
enable secret 5 $1$mERr$YsXqIF8306MddLsesE9MP/
!
!
ip dhcp excluded-address 192.168.1.1 192.168.1.7
ip dhcp excluded-address 192.168.2.1 192.168.2.7
ip dhcp excluded-address 192.168.3.1 192.168.3.7
ip dhcp excluded-address 192.168.4.1 192.168.4.7
!
ip dhcp pool vlan-1-wireless
 network 192.168.1.0 255.255.255.0
 default-router 192.168.1.1
ip dhcp pool vlan-2-pc-encargados
 network 192.168.2.0 255.255.255.0
 default-router 192.168.2.1
ip dhcp pool vlan-3-disp-admins
 network 192.168.3.0 255.255.255.0
 default-router 192.168.3.1
ip dhcp pool vlan-4-disp-servicios
 network 192.168.4.0 255.255.255.0
 default-router 192.168.4.1
!
!
ip routing
!
!
!
!
username netadmin password 0 netadmin
!
!
!
!
!
!
!
!
!
!
ip ssh version 2
ip domain-name cisco.com
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/1
 description dist-P1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/2
 description dist-PB
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/3
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/4
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/5
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/6
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/7
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/8
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/9
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/10
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/11
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/12
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/13
 switchport access vlan 4
!
interface FastEthernet0/14
 switchport access vlan 4
!
interface FastEthernet0/15
 switchport access vlan 3
!
interface FastEthernet0/16
 switchport access vlan 3
!
interface FastEthernet0/17
 switchport access vlan 2
!
interface FastEthernet0/18
 switchport access vlan 2
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
 switchport access vlan 5
!
interface FastEthernet0/22
 switchport access vlan 5
!
interface FastEthernet0/23
 switchport access vlan 5
!
interface FastEthernet0/24
 switchport access vlan 5
!
interface GigabitEthernet0/1
!
interface GigabitEthernet0/2
!
interface Vlan1
 description vlan-1-wireless
 ip address 192.168.1.1 255.255.255.0
!
interface Vlan2
 description vlan-2-PC-encargados
 mac-address 0002.1650.db01
 ip address 192.168.2.1 255.255.255.0
!
interface Vlan3
 description vlan-3-disp-admins
 mac-address 0002.1650.db03
 ip address 192.168.3.1 255.255.255.0
!
interface Vlan4
 description vlan-4-disp-servicios
 mac-address 0002.1650.db04
 ip address 192.168.4.1 255.255.255.0
!
interface Vlan5
 description vlan-5-adm
 mac-address 0002.1650.db05
 ip address 192.168.5.1 255.255.255.0
!
router rip
!
ip classless
!
ip flow-export version 9
!
!
!
banner motd E
WARNING
If you don't have authorization, don't try log in E
!
!
!
!
!
line con 0
 password cisco
 logging synchronous
 login
!
line aux 0
!
line vty 0 4
 password sanjose
 login local
 transport input ssh
line vty 5 15
 password sanjose
 login local
 transport input ssh
!
!
!
!
end
