# Tarea 1. Curso de nivelación de GNU/Linux
##  Opinión sobre el curso.
Me pareció un curso bastante completo para iniciar en el mundo de Linux y el software open source, cubre los temas y comandos más relevantes de la shell de Linux desde el manejo de archivos, los usuarios y grupos, permisos hasta configuración de redes. Sinceramente es un curso que me hubiera gustado tomar en los primeros semestres de la carrera y que creo podría ser implementado en el curso propedeútico que da la facultad

## Evidencias
![Videos 1 - 5](./img/p1.png)

![Videos 6 - 10](img/p2.png)

![Videos 11 - 15](img/p3.png)
