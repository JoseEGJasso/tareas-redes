# Tarea 3. Flujo de trabajo en GitLab
- Número de cuenta: `316093837`
- Usuario de GitLab: `@JoseEGJasso`

Hola, esta es mi carpeta para la [*tarea-3*][liga-tarea-3].

Actividades a las que me quiero dedicar al salir de la facultad:

- Administración de bases de datos
- Desarrollo de aplicaciones móviles
- Desarrollo web (principalmente back-end)
- Análisis de datos

Cosas que me gusta hacer en mi tiempo libre:

- Escuchar música
- Jugar videojuegos
- Ver videos
- Pasear con mi perrito

| ![](img/lomito.jpg)       |
|:---------------------------:|
| Esta es la imagen que elegí |

[liga-tarea-3]: https://redes-ciencias-unam.gitlab.io/tareas/tarea-3/
