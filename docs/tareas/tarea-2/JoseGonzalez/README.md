# Tarea 2. Instalación de Cisco Packet Tracer
##  Explicación de la topología.
La red que creé simula las habitaciones de mi casa y todos los dispositivos que usamos mi familia y yo, la mayoría de dispositivos que se conectan inalámbricamente son smartphones. No incluí impresoras ni tablets porque no cuento con ellos. La IP asiganda a los dispositivos es 192.168.1.0/24

**Nota**: Por alguna razón al salir de Packet Tracer no se guardan las configuraciones de IPs de los dispositivos inalámbricos de la red, pero en las capturas muestro como es que funciona la comunicación entre ellos.

## Imágenes
* Evidencia del curso Getting Started with Cisco Packet Tracer

![](./img/curso.png)

* Ventana "acerca de" Packet Tracer

![](./img/about.png)

* Plano de la red casera

![](./img/plano.png)

* Topología de la red

![](./img/red.png)

* Configuración de red de los dispositivos

![](./img/conf1.png)

![](./img/conf2.png)

![](./img/conf3.png)

* Pruebas de conectividad

PC conectada inalámbricamente comunicándose con dispositivos conectados alámbricamente

![](./img/pc-test.png)

Smartphone conectado inalámbricamente comunicándose con dispositivos conectados inalámbricamente

![](./img/smp-test.png)
