# Tarea 4. Laboratorio virtual de redes
## Imagenes
* ### Debian
| ![](img/about-debian.png)|
|:------------------------:|
| Información del sistema  |

Ejecución de comandos como usuario `root`

```text
# uname -a
Linux debian 5.10.0-17-amd64 #1 SMP Debian 5.10.136-1 (2022-08-13) x86_64 GNU/Linux

# cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"

# cat /etc/debian_version
11.4

# lsmod
Module                  Size  Used by
vboxsf                 90112  1
vboxvideo              49152  0
rfkill                 32768  3
snd_intel8x0           49152  2
joydev                 28672  0
snd_ac97_codec        180224  1 snd_intel8x0
vboxguest             421888  7 vboxsf
ac97_bus               16384  1 snd_ac97_codec
snd_pcm               143360  2 snd_intel8x0,snd_ac97_codec
snd_timer              49152  1 snd_pcm
snd                   110592  8 snd_intel8x0,snd_timer,snd_ac97_codec,snd_pcm
soundcore              16384  1 snd
sg                     36864  0
pcspkr                 16384  0
serio_raw              20480  0
evdev                  28672  10
ac                     16384  0
nfnetlink              20480  0
msr                    16384  0
parport_pc             40960  0
ppdev                  24576  0
lp                     20480  0
parport                73728  3 parport_pc,lp,ppdev
fuse                  167936  3
configfs               57344  1
ip_tables              36864  0
x_tables               53248  1 ip_tables
autofs4                53248  2
ext4                  937984  1
crc16                  16384  1 ext4
mbcache                16384  1 ext4
jbd2                  151552  1 ext4
crc32c_generic         16384  0
hid_generic            16384  0
usbhid                 65536  0
hid                   147456  2 usbhid,hid_generic
vmwgfx                380928  6
ttm                   114688  2 vmwgfx,vboxvideo
drm_kms_helper        278528  2 vmwgfx,vboxvideo
sd_mod                 61440  3
t10_pi                 16384  1 sd_mod
crc_t10dif             20480  1 t10_pi
sr_mod                 28672  0
cdrom                  73728  1 sr_mod
crct10dif_generic      16384  1
crct10dif_common       16384  2 crct10dif_generic,crc_t10dif
ata_generic            16384  0
cec                    61440  1 drm_kms_helper
ohci_pci               20480  0
ahci                   40960  2
ehci_pci               20480  0
libahci                45056  1 ahci
ata_piix               36864  0
ohci_hcd               61440  1 ohci_pci
ehci_hcd               98304  1 ehci_pci
drm                   626688  10 vmwgfx,drm_kms_helper,vboxvideo,ttm
libata                294912  4 ata_piix,libahci,ahci,ata_generic
crc32c_intel           24576  2
psmouse               184320  0
i2c_piix4              28672  0
usbcore               331776  5 ohci_hcd,ehci_pci,usbhid,ehci_hcd,ohci_pci
usb_common             16384  3 ohci_hcd,usbcore,ehci_hcd
e1000                 163840  0
scsi_mod              270336  4 sd_mod,libata,sg,sr_mod
battery                24576  0
video                  57344  0
button                 24576  0

# ps afx
    PID TTY      STAT   TIME COMMAND
      2 ?        S      0:00 [kthreadd]
      3 ?        I<     0:00  \_ [rcu_gp]
      4 ?        I<     0:00  \_ [rcu_par_gp]
      6 ?        I<     0:00  \_ [kworker/0:0H-events_highpri]
      8 ?        I<     0:00  \_ [mm_percpu_wq]
      9 ?        S      0:00  \_ [rcu_tasks_rude_]
     10 ?        S      0:00  \_ [rcu_tasks_trace]
     11 ?        S      0:00  \_ [ksoftirqd/0]
     12 ?        R      0:00  \_ [rcu_sched]
     13 ?        S      0:00  \_ [migration/0]
     14 ?        S      0:00  \_ [watchdog/0]
     15 ?        S      0:00  \_ [cpuhp/0]
     17 ?        S      0:00  \_ [kdevtmpfs]
     18 ?        I<     0:00  \_ [netns]
     19 ?        S      0:00  \_ [kauditd]
     20 ?        S      0:00  \_ [khungtaskd]
     21 ?        S      0:00  \_ [oom_reaper]
     22 ?        I<     0:00  \_ [writeback]
     23 ?        S      0:00  \_ [kcompactd0]
     24 ?        SN     0:00  \_ [ksmd]
     25 ?        SN     0:00  \_ [khugepaged]
     26 ?        I<     0:00  \_ [crypto]
     27 ?        I<     0:00  \_ [kintegrityd]
     28 ?        I<     0:00  \_ [kblockd]
     29 ?        I<     0:00  \_ [blkcg_punt_bio]
     31 ?        I<     0:00  \_ [tpm_dev_wq]
     32 ?        I<     0:00  \_ [md]
     33 ?        I<     0:00  \_ [edac-poller]
     34 ?        S      0:00  \_ [watchdogd]
     35 ?        I<     0:00  \_ [kworker/0:1H-kblockd]
     50 ?        S      0:00  \_ [kswapd0]
    152 ?        I<     0:00  \_ [kthrotld]
    153 ?        I<     0:00  \_ [acpi_thermal_pm]
    154 ?        I<     0:00  \_ [kmpath_rdacd]
    155 ?        I<     0:00  \_ [kaluad]
    156 ?        I<     0:00  \_ [ipv6_addrconf]
    158 ?        I<     0:00  \_ [kstrp]
    227 ?        I<     0:00  \_ [ipmi-msghandler]
    370 ?        I<     0:00  \_ [iprt-VBoxWQueue]
    383 ?        I<     0:00  \_ [ata_sff]
    385 ?        S      0:00  \_ [scsi_eh_0]
    386 ?        I<     0:00  \_ [scsi_tmf_0]
    387 ?        S      0:00  \_ [scsi_eh_1]
    388 ?        I<     0:00  \_ [scsi_tmf_1]
    390 ?        I      0:00  \_ [kworker/u2:3-events_unbound]
    391 ?        S      0:00  \_ [scsi_eh_2]
    392 ?        I<     0:00  \_ [scsi_tmf_2]
    394 ?        I<     0:00  \_ [ttm_swap]
    396 ?        S      0:00  \_ [irq/18-vmwgfx]
    397 ?        S      0:00  \_ [card0-crtc0]
    399 ?        S      0:00  \_ [card0-crtc1]
    400 ?        S      0:00  \_ [card0-crtc2]
    401 ?        S      0:00  \_ [card0-crtc3]
    402 ?        S      0:00  \_ [card0-crtc4]
    403 ?        S      0:00  \_ [card0-crtc5]
    404 ?        S      0:00  \_ [card0-crtc6]
    405 ?        S      0:00  \_ [card0-crtc7]
    480 ?        I<     0:00  \_ [kdmflush/253:0]
    489 ?        I<     0:00  \_ [kdmflush/253:1]
    513 ?        I<     0:00  \_ [xfsalloc]
    514 ?        I<     0:00  \_ [xfs_mru_cache]
    515 ?        I<     0:00  \_ [xfs-buf/dm-0]
    516 ?        I<     0:00  \_ [xfs-conv/dm-0]
    517 ?        I<     0:00  \_ [xfs-cil/dm-0]
    518 ?        I<     0:00  \_ [xfs-reclaim/dm-]
    519 ?        I<     0:00  \_ [xfs-gc/dm-0]
    521 ?        I<     0:00  \_ [xfs-log/dm-0]
    522 ?        S      0:00  \_ [xfsaild/dm-0]
    713 ?        I<     0:00  \_ [xfs-buf/sda1]
    714 ?        I<     0:00  \_ [xfs-conv/sda1]
    715 ?        I<     0:00  \_ [xfs-cil/sda1]
    716 ?        I<     0:00  \_ [xfs-reclaim/sda]
    717 ?        I<     0:00  \_ [xfs-gc/sda1]
    719 ?        I<     0:00  \_ [xfs-log/sda1]
    720 ?        S      0:00  \_ [xfsaild/sda1]
    758 ?        I<     0:00  \_ [rpciod]
    759 ?        I<     0:00  \_ [xprtiod]
   3270 ?        I      0:00  \_ [kworker/0:2-ata_sff]
   3312 ?        I      0:00  \_ [kworker/0:1-ata_sff]
   3344 ?        I      0:00  \_ [kworker/u2:1-events_unbound]
   3377 ?        R      0:00  \_ [kworker/0:0-events]
      1 ?        Ss     0:01 /usr/lib/systemd/systemd --switched-root --system --deseri
    619 ?        Ss     0:00 /usr/lib/systemd/systemd-journald
    657 ?        Ss     0:00 /usr/lib/systemd/systemd-udevd
    741 ?        Ss     0:00 /usr/bin/rpcbind -w -f
    744 ?        S<sl   0:00 /sbin/auditd
    746 ?        S<     0:00  \_ /usr/sbin/sedispatch
    774 ?        SNs    0:00 /usr/sbin/alsactl -s -n 19 -c -E ALSA_CONFIG_PATH=/etc/als
    776 ?        Ssl    0:05 /usr/lib/polkit-1/polkitd --no-debug
    777 ?        Ssl    0:01 /usr/bin/dbus-daemon --system --address=systemd: --nofork
    779 ?        Ss     0:00 /usr/sbin/sssd -i --logger=files
    813 ?        S      0:00  \_ /usr/libexec/sssd/sssd_be --domain implicit_files --ui
    819 ?        S      0:00  \_ /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=fi
    783 ?        SNsl   0:00 /usr/libexec/rtkit-daemon
    786 ?        Ss     0:00 /usr/lib/systemd/systemd-machined
    787 ?        Ss     0:00 /usr/bin/lsmd -d
    788 ?        Ss     0:00 /usr/sbin/smartd -n -q never
    793 ?        Ssl    0:00 /usr/libexec/udisks2/udisksd
    795 ?        Ss     0:00 avahi-daemon: running [centos-8.local]
    812 ?        S      0:00  \_ avahi-daemon: chroot helper
    799 ?        S      0:00 /usr/sbin/chronyd
    802 ?        S      0:00 /bin/bash /usr/sbin/ksmtuned
   3416 ?        S      0:00  \_ sleep 60
    828 ?        Ssl    0:00 /usr/sbin/ModemManager
    829 ?        Ssl    0:00 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofo
    909 ?        Ss     0:00 /usr/lib/systemd/systemd-logind
    910 ?        Ssl    0:00 /usr/libexec/accounts-daemon
    974 ?        Ssl    0:00 /usr/sbin/NetworkManager --no-daemon
    982 ?        Ss     0:00 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha2
    984 ?        Ssl    0:05 /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
    988 ?        Ss     0:00 /usr/sbin/cupsd -l
    994 ?        Ssl    0:00 /usr/sbin/gssproxy -D
   1248 ?        Ssl    0:00 /usr/sbin/rsyslogd -n
   1259 ?        Ss     0:00 /usr/sbin/atd -f
   1260 ?        Ss     0:00 /usr/sbin/crond -n
   1557 ?        S      0:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/def
   1561 ?        S      0:00  \_ /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq
   1816 ?        Ssl    0:00 /usr/sbin/gdm
   2159 ?        Sl     0:00  \_ gdm-session-worker [pam/gdm-password]
   2205 tty2     Ssl+   0:00      \_ /usr/libexec/gdm-wayland-session --register-sessio
   2212 tty2     Sl+    0:00          \_ /usr/libexec/gnome-session-binary
   2272 tty2     Rl+    0:10              \_ /usr/bin/gnome-shell
   2296 tty2     S+     0:00              |   \_ /usr/bin/Xwayland :0 -rootless -termin
   2321 tty2     Sl     0:00              |   \_ ibus-daemon --xim --panel disable
   2326 tty2     Sl     0:00              |       \_ /usr/libexec/ibus-dconf
   2328 tty2     Sl     0:00              |       \_ /usr/libexec/ibus-extension-gtk3
   2473 tty2     Sl     0:00              |       \_ /usr/libexec/ibus-engine-simple
   2416 tty2     Sl+    0:00              \_ /usr/libexec/gsd-power
   2417 tty2     Sl+    0:00              \_ /usr/libexec/gsd-print-notifications
   2420 tty2     Sl+    0:00              \_ /usr/libexec/gsd-rfkill
   2425 tty2     Sl+    0:00              \_ /usr/libexec/gsd-screensaver-proxy
   2434 tty2     Sl+    0:00              \_ /usr/libexec/gsd-sharing
   2438 tty2     Sl+    0:00              \_ /usr/libexec/gsd-sound
   2441 tty2     Sl+    0:00              \_ /usr/libexec/gsd-xsettings
   2444 tty2     Sl+    0:00              \_ /usr/libexec/gsd-wacom
   2447 tty2     Sl+    0:00              \_ /usr/libexec/gsd-smartcard
   2454 tty2     Sl+    0:00              \_ /usr/libexec/gsd-account
   2474 tty2     Sl+    0:00              \_ /usr/libexec/gsd-a11y-settings
   2478 tty2     Sl+    0:00              \_ /usr/libexec/gsd-clipboard
   2483 tty2     Sl+    0:00              \_ /usr/libexec/gsd-color
   2489 tty2     Sl+    0:00              \_ /usr/libexec/gsd-datetime
   2492 tty2     Sl+    0:00              \_ /usr/libexec/gsd-housekeeping
   2498 tty2     Sl+    0:00              \_ /usr/libexec/gsd-keyboard
   2502 tty2     Sl+    0:00              \_ /usr/libexec/gsd-media-keys
   2508 tty2     Sl+    0:00              \_ /usr/libexec/gsd-mouse
   2602 tty2     Sl+    0:00              \_ /usr/bin/gnome-software --gapplication-ser
   2609 tty2     Sl+    0:00              \_ /usr/libexec/gsd-disk-utility-notify
   2633 tty2     SNl+   0:00              \_ /usr/libexec/tracker-miner-apps
   2637 tty2     SNl+   0:00              \_ /usr/libexec/tracker-miner-fs
   1833 ?        Sl     0:00 /usr/sbin/VBoxService --pidfile /var/run/vboxadd-service.s
   1984 ?        Ssl    0:00 /usr/libexec/upowerd
   1997 ?        Ss     0:00 /usr/sbin/wpa_supplicant -c /etc/wpa_supplicant/wpa_suppli
   1998 ?        Ssl    0:06 /usr/libexec/packagekitd
   2079 ?        Ssl    0:00 /usr/libexec/colord
   2172 ?        Ss     0:00 /usr/lib/systemd/systemd --user
   2176 ?        S      0:00  \_ (sd-pam)
   2189 ?        S<sl   0:00  \_ /usr/bin/pulseaudio --daemonize=no --log-target=journa
   2207 ?        Ssl    0:00  \_ /usr/bin/dbus-daemon --session --address=systemd: --no
   2283 ?        Ssl    0:00  \_ /usr/libexec/gvfsd
   2293 ?        Sl     0:00  \_ /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f -o big_
   2308 ?        Ssl    0:00  \_ /usr/libexec/at-spi-bus-launcher
   2313 ?        Sl     0:00  |   \_ /usr/bin/dbus-daemon --config-file=/usr/share/defa
   2318 ?        Sl     0:00  \_ /usr/libexec/at-spi2-registryd --use-gnome-session
   2324 ?        Ssl    0:00  \_ /usr/libexec/xdg-permission-store
   2340 ?        Sl     0:00  \_ /usr/libexec/ibus-portal
   2346 ?        Sl     0:00  \_ /usr/libexec/gnome-shell-calendar-server
   2353 ?        Ssl    0:00  \_ /usr/libexec/evolution-source-registry
   2360 ?        Sl     0:00  \_ /usr/libexec/dconf-service
   2369 ?        Sl     0:00  \_ /usr/libexec/goa-daemon
   2372 ?        Ssl    0:00  \_ /usr/libexec/gvfs-udisks2-volume-monitor
   2380 ?        Ssl    0:00  \_ /usr/libexec/gvfs-mtp-volume-monitor
   2385 ?        Ssl    0:00  \_ /usr/libexec/gvfs-gphoto2-volume-monitor
   2394 ?        Ssl    0:00  \_ /usr/libexec/gvfs-afc-volume-monitor
   2398 ?        Sl     0:00  \_ /usr/libexec/goa-identity-service
   2403 ?        Ssl    0:00  \_ /usr/libexec/gvfs-goa-volume-monitor
   2433 ?        Ssl    0:00  \_ /usr/libexec/evolution-calendar-factory
   2479 ?        Sl     0:00  |   \_ /usr/libexec/evolution-calendar-factory-subprocess
   2534 ?        Ssl    0:00  \_ /usr/libexec/evolution-addressbook-factory
   2574 ?        Sl     0:00  |   \_ /usr/libexec/evolution-addressbook-factory-subproc
   2653 ?        Ssl    0:00  \_ /usr/libexec/tracker-store
   2810 ?        Ssl    0:00  \_ /usr/libexec/gvfsd-metadata
   2884 ?        Ssl    0:01  \_ /usr/libexec/gnome-terminal-server
   2889 pts/0    Ss     0:00      \_ bash
   2948 pts/0    S      0:00          \_ sudo -i
   2957 pts/0    S      0:00              \_ -bash
   3417 pts/0    R+     0:00                  \_ ps afx
   2192 ?        Sl     0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
   2334 tty2     Sl     0:00 /usr/libexec/ibus-x11 --kill-daemon
   2412 ?        Ss     0:01 /usr/libexec/sssd/sssd_kcm --uid 0 --gid 0 --logger=files
   2547 tty2     Sl+    0:00 /usr/libexec/gsd-printer
   2632 ?        S      0:00 /usr/bin/VBoxClient --clipboard
   2635 ?        Sl     0:00  \_ /usr/bin/VBoxClient --clipboard
   2662 ?        S      0:00 /usr/bin/VBoxClient --seamless
   2665 ?        Sl     0:00  \_ /usr/bin/VBoxClient --seamless
   2675 ?        S      0:00 /usr/bin/VBoxClient --draganddrop
   2677 ?        Sl     0:03  \_ /usr/bin/VBoxClient --draganddrop
   2681 ?        S      0:00 /usr/bin/VBoxClient --vmsvga
   2684 ?        S      0:00  \_
   2812 ?        Ssl    0:00 /usr/libexec/fwupd/fwupd


# hostname -I
10.0.2.15 192.168.56.102

# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
    valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
    valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:a7:6b:4d brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
    valid_lft 85263sec preferred_lft 85263sec
    inet6 fe80::a00:27ff:fea7:6b4d/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:ad:18:3b brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.102/24 brd 192.168.56.255 scope global dynamic enp0s8
    valid_lft 527sec preferred_lft 527sec
    inet6 fe80::a00:27ff:fead:183b/64 scope link
    valid_lft forever preferred_lft forever

# ip route show
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
169.254.0.0/16 dev enp0s3 scope link metric 1000
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.102

# cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 192.168.1.254

# netstat -ntulp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      636/sshd: /usr/sbin
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      631/cupsd
tcp6       0      0 :::22                   :::*                    LISTEN      636/sshd: /usr/sbin
tcp6       0      0 ::1:631                 :::*                    LISTEN      631/cupsd
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           363/avahi-daemon: r
udp        0      0 0.0.0.0:631             0.0.0.0:*                           640/cups-browsed
udp        0      0 0.0.0.0:68              0.0.0.0:*                           476/dhclient
udp        0      0 0.0.0.0:53390           0.0.0.0:*                           363/avahi-daemon: r
udp6       0      0 :::5353                 :::*                                363/avahi-daemon: r
udp6       0      0 :::33626                :::*                                363/avahi-daemon: r

# ping -c 4 1.1.1.1
PING 1.1.1.1 (1.1.1.1): 56 data bytes
64 bytes from 1.1.1.1: icmp_seq=0 ttl=59 time=51.393 ms
64 bytes from 1.1.1.1: icmp_seq=1 ttl=59 time=47.394 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=59 time=43.553 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=59 time=46.543 ms
--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 packets received, 0% packet loss
round-trip min/avg/max/stddev = 43.553/47.221/51.393/2.800 ms

# dig example.com
; <<>> DiG 9.16.27-Debian <<>> example.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52614
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;example.com.			IN	A

;; ANSWER SECTION:
example.com.		75551	IN	A	93.184.216.34

;; Query time: 16 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Sun Sep 04 16:39:54 CDT 2022
;; MSG SIZE  rcvd: 56
```
Ejecución de comandos como usuario `jjasso`:
```bash
$ getent passwd ${jjasso}
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
tss:x:103:109:TPM software stack,,,:/var/lib/tpm:/bin/false
messagebus:x:104:110::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:105:111:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
avahi-autoipd:x:106:115:Avahi autoip daemon,,,:/var/lib/avahi-autoipd:/usr/sbin/nologin
usbmux:x:107:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
rtkit:x:108:116:RealtimeKit,,,:/proc:/usr/sbin/nologin
dnsmasq:x:109:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
avahi:x:110:117:Avahi mDNS daemon,,,:/run/avahi-daemon:/usr/sbin/nologin
speech-dispatcher:x:111:29:Speech Dispatcher,,,:/run/speech-dispatcher:/bin/false
pulse:x:112:119:PulseAudio daemon,,,:/run/pulse:/usr/sbin/nologin
saned:x:113:122::/var/lib/saned:/usr/sbin/nologin
colord:x:114:123:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/nologin
geoclue:x:115:124::/var/lib/geoclue:/usr/sbin/nologin
Debian-gdm:x:116:125:Gnome Display Manager:/var/lib/gdm3:/bin/false
jjasso:x:1000:1000:jjasso,,,:/home/jjasso:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
sshd:x:117:65534::/run/sshd:/usr/sbin/nologin
tcpdump:x:118:126::/nonexistent:/usr/sbin/nologin
vboxadd:x:998:1::/var/run/vboxadd:/bin/false


$ id
uid=1000(jjasso) gid=1000(jjasso) grupos=1000(jjasso),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),108(netdev),113(bluetooth),118(lpadmin),121(scanner),127(wireshark)


$ groups
jjasso cdrom floppy sudo audio dip video plugdev netdev bluetooth lpadmin scanner wireshark


$ sudo -l
Matching Defaults entries for jjasso on debian:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User jjasso may run the following commands on debian:
    (ALL : ALL) NOPASSWD: ALL


$ sudo -i
#
```

Verificación de la instalación de las herramientas:
```bash
$ which tcpdump nmap netcat-openbsd ngrep dsniff wget curl whois dnsutils net-tools iproute2 iptables iptables-persistent tsocks inetutils-ping inetutils-traceroute inetutils-tools ethtool
/usr/bin/tcpdump
/usr/bin/nmap
/usr/bin/ngrep
/usr/sbin/dsniff
/usr/bin/wget
/usr/bin/curl
/usr/bin/whois
/usr/sbin/iptables
/usr/bin/tsocks
/usr/bin/inetutils-traceroute
/usr/sbin/ethtool


$ whereis tcpdump nmap netcat-openbsd ngrep dsniff wget curl whois dnsutils net-tools iproute2 iptables iptables-persistent tsocks inetutils-ping inetutils-traceroute inetutils-tools ethtool
tcpdump: /usr/bin/tcpdump /usr/share/man/man8/tcpdump.8.gz
nmap: /usr/bin/nmap /usr/share/nmap /usr/share/man/man1/nmap.1.gz
netcat-openbsd:
ngrep: /usr/bin/ngrep /usr/share/man/man8/ngrep.8.gz
dsniff: /usr/sbin/dsniff /usr/share/dsniff /usr/share/man/man8/dsniff.8.gz
wget: /usr/bin/wget /usr/share/man/man1/wget.1.gz /usr/share/info/wget.info.gz
curl: /usr/bin/curl /usr/share/man/man1/curl.1.gz
whois: /usr/bin/whois /usr/share/man/man1/whois.1.gz
dnsutils:
net-tools:
iproute2: /etc/iproute2 /usr/include/iproute2
iptables: /usr/sbin/iptables /etc/iptables /usr/share/iptables /usr/share/man/man8/iptables.8.gz
iptables-persistent:
tsocks: /usr/bin/tsocks /etc/tsocks.conf /usr/share/man/man8/tsocks.8.gz /usr/share/man/man1/tsocks.1.gz
inetutils-ping:
inetutils-traceroute: /usr/bin/inetutils-traceroute /usr/share/man/man1/inetutils-traceroute.1.gz
inetutils-tools:
ethtool: /usr/sbin/ethtool /usr/share/man/man8/ethtool.8.gz
```

* ### CentOS Stream 8
| ![](img/about-centOS.png)|
|:------------------------:|
| Información del sistema  |

Ejecución de comandos como usuario `root`:

```text
# uname -a
Linux centos-8.local 4.18.0-408.el8.x86_64 #1 SMP Mon Jul 18 17:42:52 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux


# cat /etc/os-release
NAME="CentOS Stream"
VERSION="8"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="8"
PLATFORM_ID="platform:el8"
PRETTY_NAME="CentOS Stream 8"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:8"
HOME_URL="https://centos.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux 8"
REDHAT_SUPPORT_PRODUCT_VERSION="CentOS Stream"


# cat /etc/redhat-release
CentOS Stream release 8


# lsmod
Module                  Size  Used by
vboxsf                 90112  1
vboxguest             385024  2 vboxsf
vboxvideo              49152  0
drm_ttm_helper         16384  1 vboxvideo
nls_utf8               16384  1
isofs                  49152  1
cfg80211              868352  0
rfkill                 28672  4 cfg80211
uinput                 20480  0
xt_CHECKSUM            16384  0
ipt_MASQUERADE         16384  0
xt_conntrack           16384  0
ipt_REJECT             16384  0
nft_compat             20480  0
nf_nat_tftp            16384  0
nft_objref             16384  0
nf_conntrack_tftp      16384  1 nf_nat_tftp
nft_counter            16384  0
bridge                286720  0
stp                    16384  1 bridge
llc                    16384  2 bridge,stp
nft_fib_inet           16384  1
nft_fib_ipv4           16384  1 nft_fib_inet
nft_fib_ipv6           16384  1 nft_fib_inet
nft_fib                16384  3 nft_fib_ipv6,nft_fib_ipv4,nft_fib_inet
nft_reject_inet        16384  4
nf_reject_ipv4         16384  2 nft_reject_inet,ipt_REJECT
nf_reject_ipv6         16384  1 nft_reject_inet
nft_reject             16384  1 nft_reject_inet
nft_ct                 20480  9
nf_tables_set          49152  12
nft_chain_nat          16384  12
nf_nat                 45056  3 ipt_MASQUERADE,nf_nat_tftp,nft_chain_nat
nf_conntrack          172032  6 xt_conntrack,nf_nat,nf_conntrack_tftp,nft_ct,ipt_MASQUERADE,nf_nat_tftp
nf_defrag_ipv6         20480  1 nf_conntrack
nf_defrag_ipv4         16384  1 nf_conntrack
ip_set                 49152  0
nf_tables             180224  262 nft_ct,nft_compat,nft_reject_inet,nft_fib_ipv6,nft_objref,nft_fib_ipv4,nft_counter,nft_chain_nat,nf_tables_set,nft_reject,nft_fib,nft_fib_inet
nfnetlink              16384  4 nft_compat,nf_tables,ip_set
sunrpc                573440  1
snd_intel8x0           45056  3
snd_ac97_codec        143360  1 snd_intel8x0
ac97_bus               16384  1 snd_ac97_codec
intel_rapl_msr         16384  0
snd_seq                81920  0
snd_seq_device         16384  1 snd_seq
snd_pcm               126976  2 snd_intel8x0,snd_ac97_codec
intel_rapl_common      24576  1 intel_rapl_msr
snd_timer              36864  2 snd_seq,snd_pcm
snd                   102400  12 snd_seq,snd_seq_device,snd_intel8x0,snd_timer,snd_ac97_codec,snd_pcm
pcspkr                 16384  0
soundcore              16384  1 snd
i2c_piix4              24576  0
video                  49152  0
xfs                  1556480  2
libcrc32c              16384  4 nf_conntrack,nf_nat,nf_tables,xfs
sr_mod                 28672  1
sd_mod                 53248  3
t10_pi                 16384  1 sd_mod
cdrom                  65536  2 isofs,sr_mod
sg                     40960  0
ata_generic            16384  0
vmwgfx                372736  4
ttm                    73728  3 vmwgfx,vboxvideo,drm_ttm_helper
drm_kms_helper        266240  2 vmwgfx,vboxvideo
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
fb_sys_fops            16384  1 drm_kms_helper
ahci                   40960  2
libahci                40960  1 ahci
ata_piix               36864  1
crc32c_intel           24576  1
serio_raw              16384  0
drm                   585728  9 vmwgfx,drm_kms_helper,vboxvideo,drm_ttm_helper,ttm
libata                262144  4 ata_piix,libahci,ahci,ata_generic
e1000                 151552  0
dm_mirror              28672  0
dm_region_hash         20480  1 dm_mirror
dm_log                 20480  2 dm_region_hash,dm_mirror
dm_mod                151552  9 dm_log,dm_mirror
ipmi_devintf           20480  0
ipmi_msghandler       114688  1 ipmi_devintf
fuse                  155648  3


# ps afx
    PID TTY      STAT   TIME COMMAND
    2 ?        S      0:00 [kthreadd]
    3 ?        I<     0:00  \_ [rcu_gp]
    4 ?        I<     0:00  \_ [rcu_par_gp]
    6 ?        I<     0:00  \_ [kworker/0:0H-events_highpri]
    8 ?        I<     0:00  \_ [mm_percpu_wq]
    9 ?        S      0:00  \_ [rcu_tasks_rude_]
    10 ?        S      0:00  \_ [rcu_tasks_trace]
    11 ?        S      0:03  \_ [ksoftirqd/0]
    12 ?        R      0:00  \_ [rcu_sched]
    13 ?        S      0:00  \_ [migration/0]
    14 ?        S      0:00  \_ [watchdog/0]
    15 ?        S      0:00  \_ [cpuhp/0]
    17 ?        S      0:00  \_ [kdevtmpfs]
    18 ?        I<     0:00  \_ [netns]
    19 ?        S      0:00  \_ [kauditd]
    20 ?        S      0:00  \_ [khungtaskd]
    21 ?        S      0:00  \_ [oom_reaper]
    22 ?        I<     0:00  \_ [writeback]
    23 ?        S      0:00  \_ [kcompactd0]
    24 ?        SN     0:00  \_ [ksmd]
    25 ?        SN     0:01  \_ [khugepaged]
    26 ?        I<     0:00  \_ [crypto]
    27 ?        I<     0:00  \_ [kintegrityd]
    28 ?        I<     0:00  \_ [kblockd]
    29 ?        I<     0:00  \_ [blkcg_punt_bio]
    31 ?        I<     0:00  \_ [tpm_dev_wq]
    32 ?        I<     0:00  \_ [md]
    33 ?        I<     0:00  \_ [edac-poller]
    34 ?        S      0:00  \_ [watchdogd]
    35 ?        I<     0:06  \_ [kworker/0:1H-kblockd]
    49 ?        S      0:02  \_ [kswapd0]
    151 ?        I<     0:00  \_ [kthrotld]
    152 ?        I<     0:00  \_ [acpi_thermal_pm]
    153 ?        I<     0:00  \_ [kmpath_rdacd]
    154 ?        I<     0:00  \_ [kaluad]
    155 ?        I<     0:00  \_ [ipv6_addrconf]
    157 ?        I<     0:00  \_ [kstrp]
    227 ?        I<     0:00  \_ [ipmi-msghandler]
    374 ?        I<     0:00  \_ [ata_sff]
    379 ?        S      0:00  \_ [scsi_eh_0]
    382 ?        I<     0:00  \_ [scsi_tmf_0]
    384 ?        S      0:00  \_ [scsi_eh_1]
    385 ?        I<     0:00  \_ [scsi_tmf_1]
    390 ?        S      0:00  \_ [scsi_eh_2]
    392 ?        I<     0:00  \_ [scsi_tmf_2]
    396 ?        I<     0:00  \_ [ttm_swap]
    397 ?        S      0:00  \_ [irq/18-vmwgfx]
    398 ?        S      0:00  \_ [card0-crtc0]
    401 ?        S      0:00  \_ [card0-crtc1]
    402 ?        S      0:00  \_ [card0-crtc2]
    404 ?        S      0:00  \_ [card0-crtc3]
    405 ?        S      0:00  \_ [card0-crtc4]
    406 ?        S      0:00  \_ [card0-crtc5]
    407 ?        S      0:00  \_ [card0-crtc6]
    408 ?        S      0:00  \_ [card0-crtc7]
    480 ?        I<     0:00  \_ [kdmflush/253:0]
    489 ?        I<     0:00  \_ [kdmflush/253:1]
    514 ?        I<     0:00  \_ [xfsalloc]
    515 ?        I<     0:00  \_ [xfs_mru_cache]
    516 ?        I<     0:00  \_ [xfs-buf/dm-0]
    517 ?        I<     0:00  \_ [xfs-conv/dm-0]
    518 ?        I<     0:00  \_ [xfs-cil/dm-0]
    519 ?        I<     0:00  \_ [xfs-reclaim/dm-]
    520 ?        I<     0:00  \_ [xfs-gc/dm-0]
    522 ?        I<     0:00  \_ [xfs-log/dm-0]
    523 ?        S      0:00  \_ [xfsaild/dm-0]
    715 ?        I<     0:00  \_ [xfs-buf/sda1]
    716 ?        I<     0:00  \_ [xfs-conv/sda1]
    717 ?        I<     0:00  \_ [xfs-cil/sda1]
    718 ?        I<     0:00  \_ [xfs-reclaim/sda]
    719 ?        I<     0:00  \_ [xfs-gc/sda1]
    721 ?        I<     0:00  \_ [xfs-log/sda1]
    722 ?        S      0:00  \_ [xfsaild/sda1]
    754 ?        I<     0:00  \_ [rpciod]
    757 ?        I<     0:00  \_ [xprtiod]
12635 ?        I<     0:00  \_ [cfg80211]
70245 ?        I<     0:00  \_ [iprt-VBoxWQueue]
71444 ?        I      0:01  \_ [kworker/u2:1-events_unbound]
71648 ?        I      0:00  \_ [kworker/u2:2-events_unbound]
71997 ?        I      0:00  \_ [kworker/0:1-ata_sff]
72162 ?        I      0:00  \_ [kworker/0:2-ata_sff]
72220 ?        R      0:00  \_ [kworker/0:0-events_power_efficient]
    1 ?        Ss     0:03 /usr/lib/systemd/systemd --switched-root --system --deserialize 17
    620 ?        Ss     0:00 /usr/lib/systemd/systemd-journald
    660 ?        Ss     0:00 /usr/lib/systemd/systemd-udevd
    744 ?        Ss     0:00 /usr/bin/rpcbind -w -f
    746 ?        S<sl   0:00 /sbin/auditd
    748 ?        S<     0:00  \_ /usr/sbin/sedispatch
    775 ?        Ss     0:00 /usr/lib/systemd/systemd-machined
    776 ?        Ss     0:00 avahi-daemon: running [centos-8.local]
    810 ?        S      0:00  \_ avahi-daemon: chroot helper
    777 ?        SNs    0:00 /usr/sbin/alsactl -s -n 19 -c -E ALSA_CONFIG_PATH=/etc/alsa/alsactl.conf --initfile=/lib/alsa/init/00main rdaemon
    778 ?        Ss     0:00 /usr/sbin/sssd -i --logger=files
    812 ?        S      0:00  \_ /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --logger=files
    817 ?        S      0:00  \_ /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
    780 ?        Ssl    0:06 /usr/lib/polkit-1/polkitd --no-debug
    781 ?        Ss     0:00 /usr/sbin/smartd -n -q never
    787 ?        Ssl    0:00 /usr/libexec/udisks2/udisksd
    788 ?        Ssl    0:02 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
    790 ?        Ss     0:00 /usr/bin/lsmd -d
    791 ?        SNsl   0:00 /usr/libexec/rtkit-daemon
    794 ?        S      0:00 /usr/sbin/chronyd
    803 ?        S      0:00 /bin/bash /usr/sbin/ksmtuned
72229 ?        S      0:00  \_ sleep 60
    820 ?        Ssl    0:00 /usr/sbin/ModemManager
    821 ?        Ssl    0:01 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
    852 ?        Ss     0:00 /usr/lib/systemd/systemd-logind
    853 ?        Ssl    0:00 /usr/libexec/accounts-daemon
    906 ?        Ssl    0:00 /usr/sbin/NetworkManager --no-daemon
    916 ?        Ss     0:00 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc -oMACs=hmac-sha2-256-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha1,umac-128@openssh.com,hmac-sha2-512 -oGSSAPIKexAlgorithms=gss-curve25519-sha256-,gss-nistp256-sha256-,gss-group14-sha256-,gss-group16-sha512-,gss-gex-sha1-,gss-group14-sha1- -oKexAlgorithms=curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1 -oHostKeyAlgorithms=ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com -oPubkeyAcceptedKeyTypes=ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com -oCASignatureAlgorithms=ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-256,rsa-sha2-512,ssh-rsa
    917 ?        Ssl    0:36 /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
    921 ?        Ss     0:00 /usr/sbin/cupsd -l
    925 ?        Ssl    0:00 /usr/sbin/gssproxy -D
1184 ?        Ssl    0:00 /usr/sbin/rsyslogd -n
1186 ?        Ssl    0:00 /usr/sbin/gdm
2020 ?        Sl     0:00  \_ gdm-session-worker [pam/gdm-password]
2064 tty2     Ssl+   0:00      \_ /usr/libexec/gdm-wayland-session --register-session gnome-session
2070 tty2     Sl+    0:00          \_ /usr/libexec/gnome-session-binary
2133 tty2     Sl+    0:59              \_ /usr/bin/gnome-shell
2159 tty2     S+     0:00              |   \_ /usr/bin/Xwayland :0 -rootless -terminate -accessx -core -auth /run/user/1000/.mutter-Xwaylandauth.ILNAS1 -listen 4 -listen 5 -displayfd 6
2183 tty2     Sl     0:05              |   \_ ibus-daemon --xim --panel disable
2187 tty2     Sl     0:00              |   |   \_ /usr/libexec/ibus-dconf
2189 tty2     Sl     0:01              |   |   \_ /usr/libexec/ibus-extension-gtk3
2340 tty2     Sl     0:01              |   |   \_ /usr/libexec/ibus-engine-simple
71658 tty2     Sl+    0:01              |   \_ gnome-control-center
2274 tty2     Sl+    0:00              \_ /usr/libexec/gsd-power
2275 tty2     Sl+    0:00              \_ /usr/libexec/gsd-print-notifications
2278 tty2     Sl+    0:00              \_ /usr/libexec/gsd-rfkill
2281 tty2     Sl+    0:00              \_ /usr/libexec/gsd-screensaver-proxy
2287 tty2     Sl+    0:00              \_ /usr/libexec/gsd-sharing
2295 tty2     Sl+    0:00              \_ /usr/libexec/gsd-sound
2298 tty2     Sl+    0:00              \_ /usr/libexec/gsd-xsettings
2303 tty2     Sl+    0:00              \_ /usr/libexec/gsd-wacom
2305 tty2     Sl+    0:00              \_ /usr/libexec/gsd-smartcard
2307 tty2     Sl+    0:00              \_ /usr/libexec/gsd-account
2325 tty2     Sl+    0:00              \_ /usr/libexec/gsd-a11y-settings
2327 tty2     Sl+    0:00              \_ /usr/libexec/gsd-clipboard
2332 tty2     Sl+    0:00              \_ /usr/libexec/gsd-color
2336 tty2     Sl+    0:00              \_ /usr/libexec/gsd-datetime
2339 tty2     Sl+    0:00              \_ /usr/libexec/gsd-housekeeping
2347 tty2     Sl+    0:00              \_ /usr/libexec/gsd-keyboard
2349 tty2     Sl+    0:00              \_ /usr/libexec/gsd-media-keys
2358 tty2     Sl+    0:00              \_ /usr/libexec/gsd-mouse
2446 tty2     Sl+    0:00              \_ /usr/libexec/gsd-disk-utility-notify
2466 tty2     SNl+   0:00              \_ /usr/libexec/tracker-miner-apps
2470 tty2     SNl+   0:00              \_ /usr/libexec/tracker-miner-fs
2481 tty2     Sl+    0:00              \_ /usr/bin/gnome-software --gapplication-service
1188 ?        Ss     0:00 /usr/sbin/crond -n
1189 ?        Ss     0:00 /usr/sbin/atd -f
1571 ?        S      0:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
1574 ?        S      0:00  \_ /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
1836 ?        Ssl    0:00 /usr/libexec/upowerd
1848 ?        Ss     0:00 /usr/sbin/wpa_supplicant -c /etc/wpa_supplicant/wpa_supplicant.conf -u -s
1849 ?        Ssl    0:16 /usr/libexec/packagekitd
1940 ?        Ssl    0:00 /usr/libexec/colord
2031 ?        Ss     0:00 /usr/lib/systemd/systemd --user
2035 ?        S      0:00  \_ (sd-pam)
2048 ?        S<sl   0:04  \_ /usr/bin/pulseaudio --daemonize=no --log-target=journal
2066 ?        Ssl    0:00  \_ /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
2145 ?        Ssl    0:00  \_ /usr/libexec/gvfsd
2154 ?        Sl     0:00  \_ /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f -o big_writes
2170 ?        Ssl    0:00  \_ /usr/libexec/at-spi-bus-launcher
2175 ?        Sl     0:00  |   \_ /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-address 3
2180 ?        Sl     0:00  \_ /usr/libexec/at-spi2-registryd --use-gnome-session
2188 ?        Ssl    0:00  \_ /usr/libexec/xdg-permission-store
2198 ?        Sl     0:00  \_ /usr/libexec/ibus-portal
2210 ?        Sl     0:00  \_ /usr/libexec/gnome-shell-calendar-server
2218 ?        Ssl    0:00  \_ /usr/libexec/evolution-source-registry
2226 ?        Ssl    0:00  \_ /usr/libexec/gvfs-udisks2-volume-monitor
2233 ?        Sl     0:00  \_ /usr/libexec/goa-daemon
2235 ?        Ssl    0:00  \_ /usr/libexec/gvfs-mtp-volume-monitor
2243 ?        Ssl    0:00  \_ /usr/libexec/gvfs-gphoto2-volume-monitor
2249 ?        Ssl    0:00  \_ /usr/libexec/gvfs-afc-volume-monitor
2254 ?        Ssl    0:00  \_ /usr/libexec/gvfs-goa-volume-monitor
2264 ?        Sl     0:00  \_ /usr/libexec/goa-identity-service
2300 ?        Ssl    0:00  \_ /usr/libexec/evolution-calendar-factory
2344 ?        Sl     0:00  |   \_ /usr/libexec/evolution-calendar-factory-subprocess --factory all --bus-name org.gnome.evolution.dataserver.Subprocess.Backend.Calendarx2300x2 --own-path /org/gnome/evolution/dataserver/Subprocess/Backend/Calendar/2300/2
2409 ?        Sl     0:00  \_ /usr/libexec/dconf-service
2412 ?        Ssl    0:00  \_ /usr/libexec/evolution-addressbook-factory
2441 ?        Sl     0:00  |   \_ /usr/libexec/evolution-addressbook-factory-subprocess --factory all --bus-name org.gnome.evolution.dataserver.Subprocess.Backend.AddressBookx2412x2 --own-path /org/gnome/evolution/dataserver/Subprocess/Backend/AddressBook/2412/2
2482 ?        Ssl    0:00  \_ /usr/libexec/tracker-store
2655 ?        Ssl    0:12  \_ /usr/libexec/gnome-terminal-server
2670 pts/0    Ss     0:00      \_ bash
2765 pts/0    S      0:00          \_ su -
2776 pts/0    S      0:00              \_ -bash
72230 pts/0    R+     0:00                  \_ ps afx
2053 ?        Sl     0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
2195 tty2     Sl     0:00 /usr/libexec/ibus-x11 --kill-daemon
2390 tty2     Sl+    0:00 /usr/libexec/gsd-printer
70522 ?        Sl     0:01 /usr/sbin/VBoxService --pidfile /var/run/vboxadd-service.sh
71037 ?        Ss     0:03 /usr/libexec/sssd/sssd_kcm --uid 0 --gid 0 --logger=files
72037 ?        Ss     0:00 /usr/sbin/anacron -s


# hostname -I
10.0.2.15 192.168.122.1


# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f2:39:4b brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85677sec preferred_lft 85677sec
    inet6 fe80::a00:27ff:fef2:394b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:58:2f:e6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.104/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 597sec preferred_lft 597sec
    inet6 fe80::a00:27ff:fe58:2fe6/64 scope link
       valid_lft forever preferred_lft forever
4: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:1d:3d:ee brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever



# ip route show
default via 10.0.2.2 dev enp0s3 proto dhcp src 10.0.2.15 metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.104 metric 101
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown


# cat /etc/resolv.conf
# Generated by NetworkManager
search local
nameserver 192.168.1.254


# netstat -ntulp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      1571/dnsmasq
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      916/sshd
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      921/cupsd
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      1/systemd
tcp6       0      0 :::22                   :::*                    LISTEN      916/sshd
tcp6       0      0 ::1:631                 :::*                    LISTEN      921/cupsd
tcp6       0      0 :::111                  :::*                    LISTEN      1/systemd
udp        0      0 192.168.122.1:53        0.0.0.0:*                           1571/dnsmasq
udp        0      0 0.0.0.0:67              0.0.0.0:*                           1571/dnsmasq
udp        0      0 0.0.0.0:111             0.0.0.0:*                           1/systemd
udp        0      0 0.0.0.0:33973           0.0.0.0:*                           776/avahi-daemon: r
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           776/avahi-daemon: r
udp        0      0 127.0.0.1:323           0.0.0.0:*                           794/chronyd
udp6       0      0 :::52247                :::*                                776/avahi-daemon: r
udp6       0      0 :::111                  :::*                                1/systemd
udp6       0      0 :::5353                 :::*                                776/avahi-daemon: r
udp6       0      0 ::1:323                 :::*                                794/chronyd


# ping -c 4 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=59 time=47.10 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=59 time=42.0 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=59 time=44.9 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=59 time=41.5 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 41.524/44.106/47.982/2.583 ms


# dig example.com
; <<>> DiG 9.11.36-RedHat-9.11.36-4.el8 <<>> example.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27387
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;example.com.			IN	A

;; ANSWER SECTION:
example.com.		79671	IN	A	93.184.216.34

;; Query time: 19 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Sun Sep 04 15:31:17 CDT 2022
;; MSG SIZE  rcvd: 56
```

Ejecución de comandos como usuario `jjasso`:
```bash
$ getent passwd ${USER}
jjasso:x:1000:1000:jjasso:/home/jjasso:/bin/bash


$ id
uid=1000(jjasso) gid=1000(jjasso) groups=1000(jjasso),10(wheel),974(wireshark) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023


$ groups
jjasso wheel wireshark


$ sudo -l
Matching Defaults entries for jjasso on centos-8:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User jjasso may run the following commands on centos-8:
    (ALL) ALL


$ sudo -i
#
```

Verificación de la instalación de las herramientas:
```bash
$ which tcpdump nmap netcat ngrep dsniff wget curl whois bind-utils net-tools iproute iptables iptables-services iputils traceroute ethtool
/usr/sbin/tcpdump
/usr/bin/nmap
/usr/bin/netcat
/usr/sbin/ngrep
/usr/sbin/dsniff
/usr/bin/wget
/usr/bin/curl
/usr/bin/whois
/usr/bin/which: no bind-utils in (/home/jjasso/.local/bin:/home/jjasso/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin)
/usr/bin/which: no net-tools in (/home/jjasso/.local/bin:/home/jjasso/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin)
/usr/bin/which: no iproute in (/home/jjasso/.local/bin:/home/jjasso/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin)
/usr/sbin/iptables
/usr/bin/which: no iptables-services in (/home/jjasso/.local/bin:/home/jjasso/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin)
/usr/bin/which: no iputils in (/home/jjasso/.local/bin:/home/jjasso/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin)
/usr/bin/traceroute
/usr/sbin/ethtool


$ whereis tcpdump nmap netcat ngrep dsniff wget curl whois bind-utils net-tools iproute iptables iptables-services iputils traceroute ethtool
tcpdump: /usr/sbin/tcpdump /usr/share/man/man8/tcpdump.8.gz
nmap: /usr/bin/nmap /usr/share/nmap /usr/share/man/man1/nmap.1.gz
netcat: /usr/bin/netcat /usr/share/man/man1/netcat.1.gz
ngrep: /usr/sbin/ngrep /usr/share/man/man8/ngrep.8.gz
dsniff: /usr/sbin/dsniff /etc/dsniff /usr/share/man/man8/dsniff.8.gz
wget: /usr/bin/wget /usr/share/man/man1/wget.1.gz /usr/share/info/wget.info.gz
curl: /usr/bin/curl /usr/share/man/man1/curl.1.gz
whois: /usr/bin/whois.md /usr/bin/whois /etc/whois.conf /usr/share/man/man1/whois.1.gz
bind-utils:
net-tools:
iproute:
iptables: /usr/sbin/iptables /usr/libexec/iptables /usr/share/man/man8/iptables.8.gz
iptables-services:
iputils:
traceroute: /usr/bin/traceroute /usr/share/man/man8/traceroute.8.gz
ethtool: /usr/sbin/ethtool /usr/share/man/man8/ethtool.8.gz

```
